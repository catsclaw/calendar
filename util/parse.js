'use strict';

import fs from 'fs';
import { google } from 'googleapis';
import readline from 'readline';
import { simpleParser } from 'mailparser';
import path from 'path';
import base64url from 'base64url';
import fetch from 'node-fetch';
import { program } from 'commander';
import striptags from 'striptags';

import html from 'node-html-parser';

program.version('0.0.1');

const TOKEN_PATH = 'token.json';
const SCOPES = [
  'https://www.googleapis.com/auth/gmail.modify',
  'https://www.googleapis.com/auth/calendar',
];
const SUPER_SECRET_KEY = 'a#D*J88D*DJ#d38d8OQhdw8fDJ#DJ#OJD(#samkd';

function authorize(credentials, callback) {
  const {client_secret, client_id, redirect_uris} = credentials.installed;
  const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);

  fs.readFile(TOKEN_PATH, (err, token) => {
    if (err) return getNewToken(oAuth2Client, callback);
    oAuth2Client.setCredentials(JSON.parse(token));
    callback(oAuth2Client);
  });
}

function getNewToken(oAuth2Client, callback) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES,
  });
  console.log('Authorize this app by visiting this url:', authUrl);
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  rl.question('Enter the code from that page here: ', (code) => {
    rl.close();
    oAuth2Client.getToken(code, (err, token) => {
      if (err) return console.error('Error retrieving access token', err);
      oAuth2Client.setCredentials(token);
      // Store the token to disk for later program executions
      fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
        if (err) return console.error(err);
        console.log('Token stored to', TOKEN_PATH);
      });
      callback(oAuth2Client);
    });
  });
}

// const SERVER = 'https://us-central1-calendar-1531256784619.cloudfunctions.net/';
const SERVER = 'http://localhost:5001/calendar-1531256784619/us-central1/';

async function test() {
  const DAYS = '\\b(monday|tuesday|wednesday|thursday|friday|saturday|sunday|mon|tues?|wed|thur?|fri|sat|sun)?\\b';
  const DATE = '\\b(?<date>0?[1-9]|[12]\\d|3[01])(st|nd|rd|th)?\\b';
  const MONTH = '\\b(?<month>january|february|march|april|may|june|july|august|september|october|november|december|jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec|(0?[1-9]|1[0-2]))\\b ';
  const YEAR = '\\b(?<year>(20)?[1-3]\\d)\\b';
  const DATE_SEP = ' [,-./•]? ';
  const TIME = '(?<hour>[01]?\\d|2[0-3])(:(?<min>[0-5]\\d))?\\s*(?<am>[ap] \.? m )?\\b';
  const TIME2 = '(?<hour2>[01]?\\d|2[0-3])(:(?<min2>[0-5]\\d))?\\s*(?<am2>[ap] \.? m )?\\b';
  const TIME_PREAMBLE = ' \\( (?:from|until)? ';

  let phrase = MONTH + DATE + DATE_SEP + YEAR + TIME_PREAMBLE + TIME;
  let text = 'Friday, February 4, 2022\n\n(2:00 PM - 12:00 AM)'
  phrase = phrase.replace(/ /g, '\\s*');
  const regexp = new RegExp(phrase, 'i');
  const m = regexp.exec(text);
  console.log(m);

  // let body = fs.readFileSync('testemail.txt').toString();
  // body = html.parse(body);

  // const location = 'Red Onion';

  // let startDate = null;
  // let startTime = null;
  // let address = null;
  // for (let tbody of body.getElementsByTagName('tbody')) {
  //   const firstTag = tbody.getElementsByTagName('td')[0].rawText.trim();
  //   if (firstTag === 'DATE') {
  //     startDate = tbody.getElementsByTagName('td')[1].rawText.trim();
  //   }

  //   if (firstTag === 'TIME') {
  //     startTime = tbody.getElementsByTagName('td')[1].rawText.trim();
  //   }

  //   if (firstTag === location) {
  //     location = tbody.getElementsByTagName('td')[1].rawText.trim();
  //   }
  // }
}

function doAuth(cb) {
  fs.readFile('./oauth-credentials.json', (err, content) => {
    if (err) return console.log('Error loading client secret file:', err);
    authorize(JSON.parse(content), cb);
  });
}

program
  .command('test')
  .description('run test')
  .action(test);

program.parse(process.argv);
