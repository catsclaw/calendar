import 'bootstrap/dist/css/bootstrap.css';
import 'v-calendar/dist/style.css';

import { createApp } from 'vue';
import App from './App.vue';

import { initIcons } from './src/icons';

const app = createApp(App);
initIcons(app).mount('#app');

window.addEventListener(
  'keyup',
  (e) => {
    if (e.key === 'q' && e.ctrlKey) window.close();
  },
  true
);

