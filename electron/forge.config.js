module.exports = {
  packagerConfig: {},
  rebuildConfig: {},
  makers: [
    {
      name: '@electron-forge/maker-snap',
      config: {
        features: {
          audio: true,
          mpris: 'com.example.mpris',
          webgl: true,
        },
        summary: 'Pretty Awesome',
      },
    },
  ],
};
