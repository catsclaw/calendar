const { app, BrowserWindow, ipcMain } = require('electron');

const path = require('path');

let defaultPosition = null;

function createWindow() {
  const mainWindow = new BrowserWindow({
    width: 400,
    height: 500,
    // frame: false,
    // titleBarStyle: 'hidden',
    resizable: false,
    icon: path.join(__dirname, 'telescope.png'),
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
    },
  });

  mainWindow.removeMenu();
  mainWindow.loadFile('dist/index.html');
  defaultPosition = mainWindow.getPosition();

  // Open the DevTools.
  // mainWindow.webContents.openDevTools()
}

ipcMain.on('resize-window-wide', (event) => {
  const browserWindow = BrowserWindow.fromWebContents(event.sender);
  browserWindow.setMinimumSize(800, 500);
  browserWindow.setSize(800, 500);
});

ipcMain.on('resize-window-narrow', (event) => {
  const browserWindow = BrowserWindow.fromWebContents(event.sender);
  browserWindow.setMinimumSize(400, 500);
  browserWindow.setSize(400, 500);
});

app.whenReady().then(() => {
  createWindow();

  app.on('activate', function () {
    if (BrowserWindow.getAllWindows().length === 0) createWindow();
  });
});

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit();
});
