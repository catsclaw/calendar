import { ref, computed, watch } from 'vue';
import { eventData, uiData, clearEventData, clearUiData, syncUiData } from './data';
import { parseEmail } from '../parsing/parse';

import emails from './page/emails';
import eventType from './page/eventType';
import expensify from './page/expensify';
import locations from './page/locations';
import startDate from './page/startDate';
import endDate from './page/endDate';
import summary from './page/summary';

let goneBack = false;

export const PAGES = {
  emails,
  eventType,
  expensify,
  locations,
  startDate,
  endDate,
  summary,
};

const calculateLocation = () => {
  if (!eventData.currentEmail) return 'emails';
  if (!eventData.category) return 'eventType';
  if (eventData.category === 'hotels' && !eventData.expense.venue) return 'expensify';
  if (!eventData.start.city) return 'locations';
  if (!eventData.start.date) return 'startDate';
  if (!eventData.end.date) return 'endDate';
  return 'summary';
};

export const currentPage = ref('emails');
export const previousPage = computed(() => {
  if (currentPage.value === 'expensify') return 'eventType';
  if (currentPage.value === 'locations') return eventData.category === 'hotels' ? 'expensify' : 'eventType';
  if (currentPage.value === 'startDate') return 'locations';
  if (currentPage.value === 'endDate') return 'startDate';
  if (currentPage.value === 'summary') return 'endDate';
  return 'emails';
});

const nextPage = computed(() => {
  if (currentPage.value === 'emails') return 'eventType';
  if (currentPage.value === 'eventType') return eventData.category === 'hotels' ? 'expensify' : 'locations';
  if (currentPage.value === 'expensify') return 'locations';
  if (currentPage.value === 'locations') return 'startDate';
  if (currentPage.value === 'startDate') return 'endDate';
  if (currentPage.value === 'endDate') return 'summary';
  return 'summary';
});

watch(currentPage, () => {
  if (PAGES[currentPage.value].arrive) PAGES[currentPage.value].arrive();
});

export const goNext = (submit) => {
  PAGES[currentPage.value].goNext(submit);
  if (goneBack) {
    currentPage.value = nextPage.value;
  } else {
    currentPage.value = calculateLocation();
  }
};

export const goBack = () => {
  PAGES[previousPage.value].goBack();
  if (goneBack) {
    currentPage.value = previousPage.value;
  } else {
    currentPage.value = calculateLocation();
  }
  goneBack = true;
};

export const currentComponent = computed(() => PAGES[currentPage.value].comp);
export const canNext = computed(() => PAGES[currentPage.value].canNext.value);
export const canBack = computed(() => PAGES[currentPage.value].canBack.value);
export const currentIcon = computed(() => PAGES[currentPage.value].currentIcon || 'fa-regular fa-circle-right');
export const showNext = computed(() => !PAGES[currentPage.value].dontShowNext);

export const clearData = () => {
  eventData.currentEmail = null;
  eventData.category = null;
  eventData.expense = {};
  eventData.start = {};
  eventData.end = {};
  clearEventData();
  clearUiData();
  goneBack = false;
  currentPage.value = 'emails';
};

export const roundTripData = () => {
  const vehicle = eventData.start.title.split(' ')[0];
  uiData.startDate = {
    date: eventData.end.date,
    title: `${vehicle} to ${eventData.start.city}`,
  };
  uiData.endDate = {
    date: null,
    title: `Arrival from ${eventData.end.city}`,
  };
  uiData.summary.roundTrip = false;

  const newStart = {
    city: eventData.end.city,
    country: eventData.end.country,
  };
  eventData.end = {
    city: eventData.start.city,
    country: eventData.start.country,
  };
  eventData.start = newStart;
  goneBack = false;
  currentPage.value = 'startDate';
};

let lastParsedId = null;

export const assignFromParse = (email, result) => {
  clearEventData();
  eventData.currentEmail = email;
  Object.assign(eventData, result);
  syncUiData();
  currentPage.value = calculateLocation();
};

watch(
  () => eventData.currentEmail,
  async (email) => {
    if (email) {
      if (email.id === lastParsedId) return;
      if (window.electronApi) window.electronApi.resizeWindowWide();
      const result = await parseEmail(email);
      lastParsedId = email.id;
      if (!result) return;
      assignFromParse(email, result);
    } else {
      if (window.electronApi) window.electronApi.resizeWindowNarrow();
    }
  }
);
