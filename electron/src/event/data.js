import { reactive, computed } from 'vue';
import { tagMessage } from '../google';
import { clearData } from './navigation';
import { getEmailPart } from '../email';

const createEventData = () => {
  return {
    currentEmail: null,
    category: null,
    expense: {},
    start: {},
    end: {},
  };
};

const createUiData = () => {
  return {
    expensify: {},
    locations: {
      start: {},
      end: {},
    },
    startDate: {},
    endDate: {},
    summary: {
      submitExpense: true,
      roundTrip: false,
    },
  };
};

export const eventData = reactive(createEventData());
export const uiData = reactive(createUiData());

export const clearEventData = () => Object.assign(eventData, createEventData());
export const clearUiData = () => Object.assign(uiData, createUiData());

export const syncUiData = () => {
  clearUiData();
  uiData.expensify.venue = eventData.expense.venue;
  uiData.expensify.price = eventData.expense.priceEntered;
  uiData.locations.start = { city: eventData.start.city, country: eventData.start.country };
  uiData.locations.end = { city: eventData.end.city, country: eventData.end.country };
  uiData.startDate = { date: eventData.start.date, title: eventData.start.title };
  uiData.endDate = { date: eventData.end.date, title: eventData.end.title };
  uiData.summary.roundTrip = !!(eventData.return);
};

export const canTag = computed(() => {
  const endCity = eventData.end.city || uiData.locations.end.city;
  const endCountry = eventData.end.country || uiData.locations.end.country;

  return [eventData.category, endCity, endCountry, eventData.currentEmail].every(Boolean);
});

export const tag = async () => {
  const endCity = eventData.end.city || uiData.locations.end.city;
  const endCountry = eventData.end.country || uiData.locations.end.country;

  await tagMessage(eventData.category, endCity, endCountry, eventData.currentEmail.threadId);
  clearData();
};

export const emailAsText = computed(() => getEmailPart(eventData.currentEmail, 'text/plain'));
export const emailAsHtml = computed(() => {
  const htmlPart = getEmailPart(eventData.currentEmail, 'text/html');
  if (htmlPart) return htmlPart;
  if (emailAsText.value) {
    return '<html><head><title></title></head><body><pre>' + emailAsText.value + '</pre></body></html>';
  }
  return '<html><head><title></title></head><body>NO HTML OR TEXT FOUND</body></html>';
});
