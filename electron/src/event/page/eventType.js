import { computed } from 'vue';
import { eventData } from '../data';

import EventType from '../../components/EventType.vue';

export default {
  comp: EventType,
  goNext: () => null,
  goBack: () => {
    eventData.category = null;
  },
  canNext: computed(() => false),
  canBack: computed(() => true),
};
