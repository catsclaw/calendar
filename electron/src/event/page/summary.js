import { computed } from 'vue';
import { eventData, uiData } from '../data';
import { clearData, roundTripData, assignFromParse } from '../navigation';
import { submitExpense } from '../../expensify';
import { tagMessage, submitEvent } from '../../google';

import Summary from '../../components/Summary.vue';

const goNext = async (submit) => {
  if (!submit) return;
  await tagMessage(eventData.category, eventData.end.city, eventData.end.country, eventData.currentEmail.threadId);
  if (eventData.category === 'hotels' && uiData.summary.submitExpense) {
    await submitExpense({
      startDate: eventData.start.date,
      endDate: eventData.end.date,
      amount: eventData.expense.amount,
      currency: eventData.expense.currency,
      venue: eventData.expense.venue,
      city: eventData.start.city,
      country: eventData.start.country,
    });
  }
  await submitEvent(eventData);

  if (uiData.summary.roundTrip) {
    uiData.summary.roundTrip = false;
    if (eventData.return) {
      assignFromParse(eventData.currentEmail, eventData.return);
    } else {
      roundTripData();
    }
  } else {
    clearData();
  }
};

export default {
  comp: Summary,
  goNext,
  goBack: () => null,
  canNext: computed(() => true),
  canBack: computed(() => true),
  currentIcon: 'fa-solid fa-floppy-disk',
  clear: () => {
    uiData.summary.submitExpense = true;
    uiData.summary.roundTrip = false;
  },
  setData: () => null,
};
