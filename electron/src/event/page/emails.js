import { computed } from 'vue';
import { eventData } from '../data';

import Emails from '../../components/Emails.vue';

export default {
  comp: Emails,
  goNext: () => null,
  goBack: () => {
    eventData.currentEmail = null;
  },
  canNext: computed(() => false),
  canBack: computed(() => false),
};
