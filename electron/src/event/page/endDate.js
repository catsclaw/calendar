import { computed } from 'vue';
import { eventData, uiData } from '../data';
import { startDateAsLuxon } from './startDate';

import EndDate from '../../components/EndDate.vue';

let suggestedTimeOfDate = null;
let suggestedTitle = null;

const arrive = () => {
  if (!uiData.endDate.date || uiData.endDate.date.getTime() === suggestedTimeOfDate) {
    const startDate = startDateAsLuxon.value;
    if (startDate) {
      if (eventData.category === 'hotels') {
        uiData.endDate.date = startDate.plus({ days: 3 }).set({ hour: 10, minutes: 0, seconds: 0 }).toJSDate();
      } else if (eventData.category === 'travel') {
        uiData.endDate.date = startDate.plus({ hours: 4 }).toJSDate();
      } else {
        uiData.endDate.date = startDate.plus({ hours: 2 }).toJSDate();
      }

      suggestedTimeOfDate = uiData.endDate.date.getTime();
    }
  }

  if (!uiData.endDate.title || uiData.endDate.title === suggestedTitle) {
    if (eventData.category === 'hotels') {
      uiData.endDate.title = `Check out from ${eventData.expense.venue || eventData.end.city}`;
    } else if (eventData.category === 'travel') {
      uiData.endDate.title = `Arrival from ${eventData.start.city}`;
    }

    suggestedTitle = uiData.endDate.title;
  }
};

const goNext = () => {
  eventData.end.date = uiData.endDate.date;
  eventData.end.title = (uiData.endDate.title || '').trim();
};

const goBack = () => {
  eventData.end.date = null;
  eventData.end.title = null;
};

export default {
  comp: EndDate,
  goNext,
  goBack,
  canNext: computed(() => true),
  canBack: computed(() => true),
  arrive,
};
