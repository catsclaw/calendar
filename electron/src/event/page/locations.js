import { computed } from 'vue';
import { eventData, uiData } from '../data';

import Locations from '../../components/Locations.vue';

const canNext = computed(() => {
  if (!uiData.locations.start.city || !uiData.locations.start.country) return false;
  if (!uiData.locations.end.city || !uiData.locations.end.country) return false;
  return true;
});

const goNext = () => {
  eventData.start = {
    city: uiData.locations.start.city,
    country: uiData.locations.start.country,
  };
  eventData.end = {
    city: uiData.locations.end.city,
    country: uiData.locations.end.country,
  };
};

const goBack = () => {
  eventData.start = {};
  eventData.end = {};
};

export default {
  comp: Locations,
  goNext,
  goBack,
  canNext,
  canBack: computed(() => true),
};
