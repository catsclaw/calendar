import { computed } from 'vue';
import { DateTime } from 'luxon';
import { eventData, uiData } from '../data';

import StartDate from '../../components/StartDate.vue';

export const startDateAsLuxon = computed(() => {
  if (!eventData.start.date) return null;
  return DateTime.fromJSDate(eventData.start.date).setZone('utc');
});

let suggestedTitle = null;

const arrive = () => {
  if (!uiData.startDate.date) {
    uiData.startDate.date = DateTime.now()
      .set({ hour: 10, minutes: 0, seconds: 0 })
      .setZone('utc', { keepLocalTime: true })
      .toJSDate();
  }

  if (!uiData.startDate.title || uiData.startDate.title === suggestedTitle) {
    if (eventData.category === 'hotels') {
      uiData.startDate.title = `Check in to ${eventData.expense.venue || eventData.start.city}`;
    } else if (eventData.category === 'travel') {
      uiData.startDate.title = `Flight to ${eventData.end.city}`;
    } else {
      uiData.startDate.title = '';
    }

    suggestedTitle = uiData.startDate.title;
  }
};

const goNext = () => {
  eventData.start.date = uiData.startDate.date;
  eventData.start.title = (uiData.startDate.title || '').trim();
};

const goBack = () => {
  eventData.start.date = null;
  eventData.start.title = null;
};

const canNext = computed(() => !!(uiData.startDate.title || '').trim());

export default {
  comp: StartDate,
  goNext,
  goBack,
  canNext,
  canBack: computed(() => true),
  arrive,
};
