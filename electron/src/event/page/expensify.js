import { computed } from 'vue';
import { eventData, uiData } from '../data';
import { parsePrice } from '../../expensify';

import Expensify from '../../components/Expensify.vue';

const parsedPrice = computed(() => {
  if (!uiData.expensify.price) return {};
  return parsePrice(uiData.expensify.price);
});

const canNext = computed(() => {
  if (!uiData.expensify.venue) return false;
  if (!parsedPrice.value.amount) return false;
  return true;
});

const goNext = () => {
  eventData.expense = {
    venue: uiData.expensify.venue,
    priceEntered: uiData.expensify.price,
    ...parsedPrice.value,
  };
};

export default {
  comp: Expensify,
  goNext,
  goBack: () => {
    eventData.expense = {};
  },
  canNext,
  canBack: computed(() => true),
  copyData: goNext,
};
