'use strict';

import { MONTHS } from './consts';

const SERVER_URL = 'https://integrations.expensify.com/Integration-Server/ExpensifyIntegrations';
const PARTNER_USER_ID = 'aa_chris_subtlety_com';
const PARTNER_USER_SECRET = '33d2d3b0edd4046e3c8ddcf6f16adb890ed7f7bf';

const REPORT_IDS = {
  2021: 83297002,
  2022: 84464589,
  2023: 84464592,
  2024: 84464598,
};

const getDateRange = (start, end) => {
  const startYear = start.getFullYear();
  const endYear = end.getFullYear();
  const startMonth = start.getMonth();
  const endMonth = end.getMonth();
  const startDay = start.getDate();
  const endDay = end.getDate();

  let s = MONTHS[startMonth];
  s += ` ${startDay}`;
  if (startYear !== endYear) s += `, ${startYear}`;
  s += '-';
  if (startMonth !== endMonth || startYear !== endYear) s += `${MONTHS[endMonth]} `;
  s += `${endDay}, ${endYear}`;
  return s;
};

const CURRENCIES = {
  '£': 'GBP',
  $: 'USD',
  '€': 'EUR',
  lei: 'RON',
  US$: 'USD',
  R$: 'BRL',
};

export const parsePrice = (s) => {
  let m = /(?<bc>[^\d\s]+)?\s*(?<amount>[\d,.]+)\s*(?<ac>[^\d\s]+)?/.exec(s);
  if (!m) return { amount: null, currency: null };

  let amount = m.groups.amount;
  amount = amount.replaceAll(',', '');
  amount = parseFloat(amount);
  let currency = m.groups.bc || m.groups.ac;
  currency = CURRENCIES[currency] || currency || 'USD';
  currency = currency.toUpperCase();
  return { amount, currency };
};

export const submitExpense = async (args) => {
  let { startDate, endDate, amount, currency, venue, city, country } = args;

  startDate = new Date(startDate);
  endDate = new Date(endDate);
  amount = Math.ceil(amount * 100);

  const data = {
    type: 'create',
    credentials: {
      partnerUserID: PARTNER_USER_ID,
      partnerUserSecret: PARTNER_USER_SECRET,
    },
    inputSettings: {
      type: 'expenses',
      employeeEmail: 'chris@subtlety.com',
      transactionList: [
        {
          created: startDate.toISOString().substring(0, 10),
          currency,
          merchant: venue,
          amount,
          category: 'Lodging',
          comment: getDateRange(startDate, endDate) + ', ' + city,
        },
      ],
    },
  };

  if (country !== 'United States') data.inputSettings.transactionList[0].reportID = REPORT_IDS[startDate.getFullYear()];

  await fetch(SERVER_URL, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: 'requestJobDescription=' + JSON.stringify(data),
  });
};
