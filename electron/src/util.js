const REPL = {
  å: 'a',
  ã: 'a',
  à: 'a',
  á: 'a',
  ä: 'a',
  â: 'a',
  ẽ: 'e',
  è: 'e',
  é: 'e',
  ë: 'e',
  ê: 'e',
  ì: 'i',
  í: 'i',
  ï: 'i',
  î: 'i',
  õ: 'o',
  ò: 'o',
  ó: 'o',
  ö: 'o',
  ô: 'o',
  ù: 'u',
  ú: 'u',
  ü: 'u',
  û: 'u',
  ñ: 'n',
  ç: 'c',
  ş: 's',
};

export const getSearchTerm = (s) => {
  if (!s) return '';
  s = s.toLowerCase();
  for (let x of s) s = s.replaceAll(x, REPL[x] || x);
  s = s.replace(/[^a-z0-9 ]/g, '');
  s = s.replace(/\s+/g, ' ');
  s = s.trim();
  return s;
};
