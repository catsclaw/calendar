import { getTimeZoneFromLocation, getGeocodedAddress } from '../../google';
import striptags from 'striptags';

import { readDate, MONTH, DATE, YEAR, TIME } from '../regex';

const DMY = DATE + ' ' + MONTH + ' ,? ' + YEAR + ' (?:at)? ' + TIME;
const MDY = MONTH + ' ' + DATE + ' ,? ' + YEAR + ' (?:at)? ' + TIME;

let OPEN_TABLE_DATES = [
  MDY,
  DMY,
];

export default async (email) => {
  const $ = email.parsed;

  if (email.from.includes('no-reply@opentable.co.uk')) {
    OPEN_TABLE_DATES = [
      DMY,
      MDY,
    ];
  } else if (!email.from.includes('no-reply@opentable.com')) {
    return null;
  }

  let addr = {};
  let dateString = null;
  let datetime = null;
  let restName = null;
  for (let e of $('span')) {
    e = $(e);
    const id = e.attr('id');

    if (/address/.exec(id)) addr = await getGeocodedAddress(striptags(e.html()));
    if (/reservation-datetime/.exec(id)) dateString = striptags(e.html());
    if (/restaurant-name/.exec(id)) restName = striptags(e.html());
  }

  let startPhrase = 'Reservation';
  if (dateString) {
    for (let phrase of OPEN_TABLE_DATES) {
      phrase = phrase.replace(/ /g, '\\s*');
      const regexp = new RegExp(phrase, 'i');
      const m = regexp.exec(dateString);
      if (m) {
        datetime = readDate(m);
        if (datetime.hour > 16) {
          startPhrase = 'Dinner';
        } else if (datetime.weekday > 5 && datetime.hour > 9 && datetime.hour < 16) {
          startPhrase = 'Brunch';
        } else if (datetime.hour > 10) {
          startPhrase = 'Lunch';
        } else if (datetime.hour > 6) {
          startPhrase = 'Breakfast';
        }

        break;
      }
    }
  }

  const info = {
    category: 'events',
    start: {
      ...addr,
    },
    end: {
      ...addr,
    },
  };

  if (restName) {
    info.start.title = `${startPhrase} at ${restName}`;
  }

  if (datetime) {
    info.start.date = datetime.toJSDate();
    info.end.date = datetime.plus({ hours: 1 }).toJSDate();
  }

  return info;
};
