import { MONTH, DATE, DATE_SEP, YEAR, TIME, readDate } from '../regex';
import { getGeocodedAddress } from '../../google';
import { parsePrice } from '../../expensify';

export default async (email) => {
  const $ = email.parsed;
  if (!$) return;

  const m = new RegExp(/Thanks! Your booking is confirmed at (?<loc>.+)/, 'iu').exec(email.subject);
  if (!m) return null;

  if (!$('address').html()) return null;
  const addr = await getGeocodedAddress($('address').html().trim());

  let startTime = null;
  let endTime = null;
  let price = null;
  for (let table of $('table')) {
    table = $(table);
    const text = table.text().trim();

    if (text === 'Check-in') startTime = table.parent().children().last().text().trim();
    if (text === 'Check-out') endTime = table.parent().children().last().text().trim();
    if (text.match(/^Total Price$/i) && !price) price = table.parent().children().last().text().trim();
  }

  const parsedPrice = price ? parsePrice(price) : {};

  return {
    category: 'hotels',
    expense: {
      priceEntered: price,
      venue: m.groups.loc,
      ...parsedPrice,
    },
    start: {
      ...addr,
      date: parseDate(startTime, false).toJSDate(),
      title: `Check in to ${m.groups.loc}`,
    },
    end: {
      ...addr,
      date: parseDate(endTime, true).toJSDate(),
      title: `Check out from ${m.groups.loc}`,
    },
  };
};

const TIME_PREAMBLE = ' \\( (?:from|until)? ';

const CHECK_IN_DATE_FORMATS = [
  MONTH + DATE + DATE_SEP + YEAR + TIME_PREAMBLE + TIME,
  DATE + MONTH + DATE_SEP + YEAR + TIME_PREAMBLE + TIME,
];

const CHECK_OUT_DATE_FORMATS = [
  MONTH + DATE + DATE_SEP + YEAR + '[^-]+- ' + TIME,
  MONTH + DATE + DATE_SEP + YEAR + TIME_PREAMBLE + TIME,
  DATE + MONTH + DATE_SEP + YEAR + '[^-]+- ' + TIME,
  DATE + MONTH + DATE_SEP + YEAR + TIME_PREAMBLE + TIME,
];

const parseDate = (text, checkout) => {
  const DATE_FORMATS = checkout ? CHECK_OUT_DATE_FORMATS : CHECK_IN_DATE_FORMATS;
  for (let phrase of DATE_FORMATS) {
    phrase = phrase.replace(/ /g, '\\s*');
    const regexp = new RegExp(phrase, 'i');
    const m = regexp.exec(text);
    if (m) return readDate(m);
  }

  return null;
};
