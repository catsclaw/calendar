import ical from 'ical';
import { getEmailAttachment } from '../../email';
import { getLocationAndTz } from '../../google';
import { DateTime } from 'luxon';

export default async (email) => {
  if (!email.from.includes('auto-confirm@info.thetrainline.com')) return null;
  const ics = await getEmailAttachment(email.msg, 'text/calendar');
  const icalEvents = Object.values(ical.parseICS(ics) || {});
  if (icalEvents.length === 0) return null;
  const event = icalEvents[0];

  const m = /^Your booking confirmation for (?<start>.+) to (?<end>.+) \(/.exec(email.subject);
  if (!m) return null;


  const [startAddr, endAddr] = await Promise.all([getLocationAndTz(m.groups.start), getLocationAndTz(m.groups.end)]);
  const startTime = DateTime.fromJSDate(event.start).toJSDate();


  const info = {
    category: 'travel',
    start: {
      ...startAddr[0],
      date: DateTime.fromJSDate(event.start).setZone(startAddr[1]).setZone('utc', { keepLocalTime: true }).toJSDate(),
    },
    end: {
      ...endAddr[0],
      date: DateTime.fromJSDate(event.end).setZone(endAddr[1]).setZone('utc', { keepLocalTime: true }).toJSDate(),
    },
  }

  if (startAddr[0].city) {
    info.end.title = `Arrival from ${startAddr[0].city}`;
  }

  if (endAddr[0].city) {
    info.start.title = `Train to ${endAddr[0].city}`;
  }

  return info;
};
