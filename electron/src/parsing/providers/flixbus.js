import { getLocationAndTz } from '../../google';
import { DateTime } from 'luxon';

export default async (email) => {
  if (!email.subject.startsWith('Booking Confirmation ')) return;
  if (!email.from.includes('booking.flixbus.com')) return;

  const $ = email.parsed;
  if (!$) return null;

  const h2 = $('h2');
  if (h2.length < 2) return null;
  const [startCity, endCity] = $(h2[1]).text().split(' - ');
  const startTime = DateTime.fromISO($($('time')[1]).attr().datetime);
  const endTime = DateTime.fromISO($($('time')[2]).attr().datetime);

  const [startAddr, endAddr] = await Promise.all([getLocationAndTz(startCity), getLocationAndTz(endCity)]);

  return {
    category: 'travel',
    start: {
      ...startAddr[0],
      date: startTime.setZone(startAddr[1]).setZone('utc', { keepLocalTime: true }).toJSDate(),
      title: `Bus to ${endCity}`,
    },
    end: {
      ...endAddr[0],
      date: endTime.setZone(endAddr[1]).setZone('utc', { keepLocalTime: true }).toJSDate(),
      title: `Arrival from ${startCity}`,
    },
  };
};
