import { DateTime } from 'luxon';
import { getLocationAndTz } from '../../google';

export default async (email) => {
  const $ = email.parsed;
  if (!$) return;

  if (!email.subject.startsWith('Travel Reservation Center Trip ID')) return null;

  let parentNode = null;
  for (let span of $('span')) {
    span = $(span);

    const text = span.text().trim();
    if (text === 'Departing Flight') {
      parentNode = span;
      for (let x = 0; x < 7; x++) {
        parentNode = parentNode.parent();
      }

      break;
    }
  }

  if (!parentNode) return null;

  let inDepart = true;
  let departNode = {};
  let returnNode = {};

  const breakNode = (n) => {
    const timeText = $(n.children()[2]).text();
    return {
      city: n.children().first().text(),
      datetime: n.children().last().text().substr(5) + ' ' + timeText.substr(0, timeText.length - 3),
    };
  };

  for (let tr of $('tr')) {
    tr = $(tr);

    if (tr.text().includes('Returning Flight')) {
      if (tr.text().includes('Departing Flight')) continue;
      inDepart = false;
    }

    for (let i of ($('img', tr))) {
      i = $(i);
      if (i.attr('src').endsWith('FlightTimeStart.gif')) {
        const row = i.parent().parent().children();
        if (!departNode.start && inDepart) departNode.start = breakNode(row.first());
        if (inDepart) departNode.end = breakNode(row.last());
        if (!returnNode.start && !inDepart) returnNode.start = breakNode(row.first());
        if (!inDepart) returnNode.end = breakNode(row.last());
      }
    }
  }

  if (!departNode.start.city) return null;

  const [startAddr, endAddr, startAddr2, endAddr2] = await Promise.all([
    getLocationAndTz(departNode.start.city),
    getLocationAndTz(departNode.end.city),
    getLocationAndTz(returnNode.start ? returnNode.start.city : null),
    getLocationAndTz(returnNode.end ? returnNode.end.city : null),
  ]);

  const parseTime = (t) => DateTime.fromFormat(t, 'MMM d h:mm a z');

  const startTime = parseTime(departNode.start.datetime + ' ' + startAddr[1]);
  const endTime = parseTime(departNode.end.datetime + ' ' + endAddr[1]);
  const startTime2 = returnNode.start ? parseTime(returnNode.start.datetime + ' ' + startAddr2[1]) : null;
  const endTime2 = returnNode.end ? parseTime(returnNode.end.datetime + ' ' + endAddr2[1]) : null;

  const formatData = (retStartAddr, retStartTime, retEndAddr, retEndTime) => {
    return {
      category: 'travel',
      start: {
        ...retStartAddr[0],
        date: retStartTime.setZone(retStartAddr[1]).setZone('utc', { keepLocalTime: true }).toJSDate(),
        title: `Flight to ${retEndAddr[0].city}`,
      },
      end: {
        ...retEndAddr[0],
        date: retEndTime.setZone(retEndAddr[1]).setZone('utc', { keepLocalTime: true }).toJSDate(),
        title: `Arrival from ${retStartAddr[0].city}`,
      },
    };
  }

  const data = formatData(startAddr, startTime, endAddr, endTime);
  if (startTime2) data.return = formatData(startAddr2, startTime2, endAddr2, endTime2);
  return data;
};
