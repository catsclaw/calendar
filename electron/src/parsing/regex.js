import { DateTime } from 'luxon';

export const DAYS = '\\b(monday|tuesday|wednesday|thursday|friday|saturday|sunday|mon|tues?|wed|thur?|fri|sat|sun)?\\b';
export const DATE = '\\b(?<date>0?[1-9]|[12]\\d|3[01])(st|nd|rd|th)?\\b ';
export const MONTH =
  '\\b(?<month>january|february|march|april|may|june|july|august|september|october|november|december|jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec|(0?[1-9]|1[0-2]))\\b ';
export const YEAR = '\\b(?<year>(20)?[1-3]\\d)\\b';
export const DATE_SEP = ' [,-./•]? ';
export const TIME = '(?<hour>[01]?\\d|2[0-3])(:(?<min>[0-5]\\d))?\\s*(?<am>[ap] \.? m )?\\b';  // eslint-disable-line no-useless-escape
export const TIME2 = '(?<hour2>[01]?\\d|2[0-3])(:(?<min2>[0-5]\\d))?\\s*(?<am2>[ap] \.? m )?\\b';  // eslint-disable-line no-useless-escape

export const readDate = (m) => {
  const g = m.groups;

  let month = g.month;
  let date = g.date;
  let year = g.year;
  let hour = g.hour;
  let min = g.min;
  let am = g.am;

  if (!year) year = '2021';

  if (!hour) return DateTime.fromJSDate(new Date(`${year} ${month} ${date} Z`));

  min = min || '00';
  am = am || '';

  return DateTime.fromJSDate(new Date(`${year} ${month} ${date} ${hour}:${min} ${am} Z`));
};

// const parseDate = (text, leadingPhrases, datePhrases) => {
//   for (let leadingPhrase of leadingPhrases) {
//     for (let datePhrase of datePhrases) {
//       let phrase = leadingPhrase + ' ' + datePhrase;
//       phrase = phrase.replace(/ /g, '\\s*');
//       let regexp = new RegExp(phrase, 'i');
//       let m = regexp.exec(text);
//       if (m) return readDate(m.groups);
//     }
//   }

//   return null;
// };
