import cheerio from 'cheerio';

import booking from './providers/booking';
import opentable from './providers/opentable';
import flixbus from './providers/flixbus';
import trainline from './providers/trainline';
import chase from './providers/chase';
import { getSender, getEmailPart, getDate, getFrom, getSubject } from '../email';

const parsers = [booking, opentable, flixbus, trainline, chase];

export const parseEmail = async (msg) => {
  const email = {
    sender: getSender(msg),
    from: getFrom(msg),
    date: getDate(msg),
    subject: getSubject(msg),
    html: getEmailPart(msg, 'text/html'),
    text: getEmailPart(msg, 'text/plain'),
    msg,
  };
  if (email.html) email.parsed = cheerio.load(email.html);

  for (let parse of parsers) {
    const attempt = await parse(email);
    if (attempt) return attempt;
  }

  return {};
};
