import { computed } from 'vue';
import { labels } from './google';
import { STATE_ABBR } from './consts';
import { getSearchTerm } from './util';

export const cities = computed(() => {
  const cityMap = {};

  for (const l in labels.value) {
    const split = l.split('/');
    if (!['Hotels', 'Travel', 'Events'].includes(split[0])) continue;
    if (split.length < 3) continue;
    let city = null;
    const country = split[1];
    if (split.length === 4 && country === 'United States') {
      city = split[3] + ', ' + STATE_ABBR[split[2]];
    } else {
      if (country === 'United States') continue;
      city = split[2];
    }

    const searchTerm = getSearchTerm(city);
    cityMap[searchTerm] = cityMap[searchTerm] || {
      name: city,
      countries: new Set(),
    };
    cityMap[searchTerm].countries.add(getSearchTerm(country));
  }

  const sortedCities = {};
  for (const [sortName, name] of Object.entries(cityMap).sort()) {
    sortedCities[sortName] = name;
  }

  return sortedCities;
});

export const countries = computed(() => {
  const countryMap = {};

  for (const l in labels.value) {
    const split = l.split('/');
    if (!['Hotels', 'Travel', 'Events'].includes(split[0])) continue;
    if (split.length < 2) continue;
    countryMap[getSearchTerm(split[1])] = split[1];
  }

  const sortedMap = {};
  for (const [sortName, name] of Object.entries(countryMap).sort()) {
    sortedMap[sortName] = name;
  }

  return sortedMap;
});
