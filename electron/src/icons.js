import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import {
  faTag,
  faRightFromBracket,
  faCalendarXmark,
  faPlaneUp,
  faHotel,
  faTicket,
  faBan,
  faArrowRotateRight,
  faFloppyDisk,
} from '@fortawesome/free-solid-svg-icons';
import { faCircleRight, faCircleLeft } from '@fortawesome/free-regular-svg-icons';
library.add(faTag);
library.add(faRightFromBracket);
library.add(faCalendarXmark);
library.add(faPlaneUp);
library.add(faHotel);
library.add(faTicket);
library.add(faBan);
library.add(faArrowRotateRight);
library.add(faCircleRight);
library.add(faCircleLeft);
library.add(faFloppyDisk);

export const initIcons = (app) => {
  app.component('FaIcon', FontAwesomeIcon);
  return app;
};
