import { computed, ref, reactive, watch } from 'vue';
import { STATES } from './consts';
import { getSubject } from './email';
import { get, set } from 'idb-keyval';
import { DateTime } from 'luxon';

export const emailSearchText = ref('');
const cleanedSearchText = computed(() => emailSearchText.value.trim());

export const getGeocodedLocation = async (address) => {
  const addrDoc = await get(`addr-${address}`);
  if (addrDoc && addrDoc.expires > new Date()) {
    return addrDoc.addr;
  }

  const r = await doGet(GMAP_GEOCODE_ENDPOINT, {
    content_type: null,
    data: {
      key: GOOGLE_MAP_KEY,
      address,
    },
  });

  let addr = null;
  if (r.status === 'OK') {
    // // Cut out line by line and retry?
    // addr = addr.split('\n').slice(1).join('\n').trim();
    // if (!addr) return {};
    addr = r.results[0];
  }

  await set(`addr-${address}`, {
    addr,
    expires: DateTime.utc().plus({ months: 3 }).toJSDate(),
  });

  return addr;
};

export const getTimeZoneFromLocation = async (address) => {
  if (address.city && address.country) address = `${address.city}, ${address.country}`;

  const tzDoc = await get(`tz-${address}`);
  if (tzDoc && tzDoc.expires > new Date()) {
    return tzDoc.tz;
  }

  const addr = await getGeocodedLocation(address);
  if (!addr) return null;
  const latLng = addr.geometry.location;

  const r = await doGet(GMAP_TIMEZONE_ENDPOINT, {
    content_type: null,
    data: {
      key: GOOGLE_MAP_KEY,
      timestamp: 0,
      location: `${latLng.lat},${latLng.lng}`,
    },
  });
  if (!r.status === 'OK') return null;
  await set(`tz-${address}`, {
    tz: r.timeZoneId,
    expires: DateTime.utc().plus({ months: 3 }).toJSDate(),
  });
  return r.timeZoneId;
};

export const getLocationAndTz = async (address) => {
  if (!address) return null;
  const addr = await getGeocodedAddress(address);
  const tzInfo = await getTimeZoneFromLocation(address);
  return [addr, tzInfo];
}

export const getGeocodedAddress = async (address) => {
  const addr = await getGeocodedLocation(address);
  if (!addr) return {};

  const result = {};
  let postalTown = null;
  let locality = null;
  let adminArea1 = null;

  for (let d of addr.address_components) {
    if (d.types.includes('country')) result.country = d.long_name;
    if (d.types.includes('administrative_area_level_2')) result.city = d.long_name;
    if (d.types.includes('postal_town')) postalTown = d.long_name;
    if (d.types.includes('locality')) locality = d.long_name;
    if (d.types.includes('administrative_area_level_1')) adminArea1 = d.short_name;
  }

  result.city = locality || postalTown || result.city;
  if (result.country === 'United States') result.city += `, ${adminArea1}`;

  // result.streetAddress = addr.formatted_address;
  // result.name = countryReq.data.result.name;

  return {
    city: result.city,
    country: result.country,
  };
};

export const user = ref({});
export const labels = ref({});
export const recentMessages = reactive({
  messages: {},
  nextPageToken: null,
  includeArchived: false,
  fetching: false,
});

const BASE_URL = 'http://localhost:5173/dist/';

const CALENDAR_ID = '1016jeolmgaikoea566ksu5ieo@group.calendar.google.com';

const OAUTH_ENDPOINT = 'https://accounts.google.com/o/oauth2/v2/auth';
const TOKEN_ENDPOINT = 'https://oauth2.googleapis.com/token';
const GMAIL_ENDPOINT = 'https://gmail.googleapis.com/gmail/v1';
const GMAIL_MESSAGE_ENDPOINT = GMAIL_ENDPOINT + '/users/me/messages';
const GMAIL_LABELS_ENDPOINT = GMAIL_ENDPOINT + '/users/me/labels';
const GMAIL_THREADS_ENDPOINT = GMAIL_ENDPOINT + '/users/me/threads';
const GCAL_ENDPOINT = `https://www.googleapis.com/calendar/v3/calendars/${CALENDAR_ID}/events`;
const GMAP_TIMEZONE_ENDPOINT = 'https://maps.googleapis.com/maps/api/timezone/json';
const GMAP_GEOCODE_ENDPOINT = 'https://maps.googleapis.com/maps/api/geocode/json';

const CLIENT_ID = '969536222638-la98jsfg33tpjs6gbj7prn0bfrggs7sa.apps.googleusercontent.com';
const CLIENT_SECRET = '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';
const SCOPES =
  'https://www.googleapis.com/auth/gmail.labels https://www.googleapis.com/auth/gmail.modify https://www.googleapis.com/auth/calendar';
const GOOGLE_MAP_KEY = 'AIzaSyBLT_shZHPlPZOb5TRS5kFtp7sTUe7OHpI';

export const checkLogin = async () => {
  const urlParams = new URLSearchParams(window.location.search);
  const code = urlParams.get('code');
  if (code) {
    const r = await doPost(TOKEN_ENDPOINT, {
      data: {
        client_id: CLIENT_ID,
        client_secret: CLIENT_SECRET,
        code,
        grant_type: 'authorization_code',
        redirect_uri: BASE_URL,
      },
    });

    if (r.access_token) {
      window.localStorage.setItem('accessToken', r.access_token);
      window.localStorage.setItem('refreshToken', r.refresh_token);
    } else {
      window.localStorage.setItem('accessToken', '');
      window.localStorage.setItem('refreshToken', '');
    }

    window.location.href = BASE_URL;
  } else {
    let accessToken = window.localStorage.getItem('accessToken');
    const refreshToken = window.localStorage.getItem('refreshToken');

    if (refreshToken) {
      accessToken = await callRefreshToken(refreshToken);
      window.localStorage.setItem('accessToken', accessToken || '');
    }

    if (!accessToken) {
      login();
    } else {
      user.value = {
        loggedIn: true,
        accessToken: accessToken,
        refreshToken: refreshToken,
      };
    }
  }
};

const loadValues = () => {
  getLabels();
  getRecentMessages();
};

watch(user, loadValues);

let searchChangeKey = 1;

const redoSearch = async () => {
  const myKey = ++searchChangeKey;
  await new Promise((resolve) => setTimeout(resolve, 500));
  if (myKey !== searchChangeKey) return;
  getRecentMessages();
};

let toggleOnIncludeArchived = true;

watch(emailSearchText, () => {
  if (toggleOnIncludeArchived) recentMessages.includeArchived = true;
  redoSearch();
});
watch(
  () => recentMessages.includeArchived,
  (val) => {
    if (!val) toggleOnIncludeArchived = false;
    redoSearch();
  }
);

export const listMessages = async (nextPageToken, includeArchived) => {
  const data = { maxResults: 20 };
  if (nextPageToken) data.pageToken = nextPageToken;
  if (cleanedSearchText.value) data.q = cleanedSearchText.value;
  if (!includeArchived) data.labelIds = ['INBOX'];
  const myKey = searchChangeKey.value;
  const r = await doGet(GMAIL_MESSAGE_ENDPOINT, { auth: true, data });
  if (myKey !== searchChangeKey.value) return;
  recentMessages.nextPageToken = r.nextPageToken;
  return r.messages || [];
};

export const getMessage = async (messageId) => {
  return await doGet(`${GMAIL_MESSAGE_ENDPOINT}/${messageId}`, {
    auth: true,
    data: { format: 'full' },
  });
};

export const getAttachment = async (messageId, attachmentId) => {
  return await doGet(`${GMAIL_MESSAGE_ENDPOINT}/${messageId}/attachments/${attachmentId}`, {
    auth: true,
  });
};

export const getRecentMessages = async (next) => {
  if (!user.value.accessToken) {
    recentMessages.messages = {};
    recentMessages.nextPageToken = null;
    return;
  }

  let myKey = searchChangeKey;

  const messages = await listMessages(next ? recentMessages.nextPageToken : false, recentMessages.includeArchived);

  if (myKey !== searchChangeKey) return;

  const messageDetails = await Promise.all(messages.map((x) => getMessage(x.id)));

  if (!next) recentMessages.messages = {};
  for (const msg of messageDetails) {
    const subject = getSubject(msg);
    if (!recentMessages.messages[subject]) recentMessages.messages[subject] = msg;
  }
};

export const login = () => {
  const params = new URLSearchParams();
  params.append('client_id', CLIENT_ID);
  params.append('redirect_uri', BASE_URL);
  params.append('response_type', 'code');
  params.append('scope', SCOPES);
  params.append('access_type', 'offline');

  window.location.href = `${OAUTH_ENDPOINT}?${params.toString()}`;
};

export const callRefreshToken = async (token) => {
  const result = await doPost(TOKEN_ENDPOINT, {
    data: {
      client_id: CLIENT_ID,
      client_secret: CLIENT_SECRET,
      refresh_token: token,
      grant_type: 'refresh_token',
    },
  });
  return result.access_token;
};

export const logout = () => {
  user.value = {};
  window.localStorage.setItem('accessToken', '');
  window.localStorage.setItem('refreshToken', '');
  login();
};

export const getLabels = async () => {
  if (!user.value.accessToken) {
    labels.value = {};
    return;
  }

  const r = await doGet(GMAIL_LABELS_ENDPOINT, { auth: true });
  labels.value = {};
  for (const l of r.labels) {
    labels.value[l.name] = l.id;
  }

  return labels.value;
};

export const createLabels = async (newLabels) => {
  newLabels = newLabels.filter((x) => !labels.value[x]);
  const create = async (label) => {
    const r = await doPost(GMAIL_LABELS_ENDPOINT, {
      auth: true,
      data: {
        name: label,
        labelListVisibility: 'labelShow',
        messageListVisibility: 'show',
      },
    });
    return r;
  };

  newLabels = await Promise.all(newLabels.map((x) => create(x)));
  for (const l of newLabels) {
    labels.value[l.name] = l.id;
  }
};

export const tagMessage = async (category, city, country, threadId) => {
  const topLabel = category.charAt(0).toUpperCase() + category.substring(1).toLowerCase();
  let labels = [topLabel];
  labels.push(`${topLabel}/${country}`);
  if (country === 'United States') {
    let [usCity, usState] = city.split(/\s*,\s*/);
    usState = STATES[usState.toUpperCase()];
    labels.push(`${topLabel}/${country}/${usState}`);
    labels.push(`${topLabel}/${country}/${usState}/${usCity}`);
  } else {
    labels.push(`${topLabel}/${country}/${city}`);
  }

  await createLabels(labels);
  await setLabels(threadId, labels, ['INBOX', 'UNREAD']);
};

const CALENDAR_COLORS = {
  events: 3,
  hotels: 1,
  travel: 6,
};

export const submitEvent = async (eventData) => {
  const startLocation = `${eventData.start.city}, ${eventData.start.country}`;
  const endLocation = `${eventData.end.city}, ${eventData.end.country}`;
  let startTz = null;
  let endTz = null;
  if (startLocation === endLocation) {
    endTz = startTz = await getTimeZoneFromLocation(startLocation);
  } else {
    [startTz, endTz] = await Promise.all([
      getTimeZoneFromLocation(startLocation),
      getTimeZoneFromLocation(endLocation),
    ]);
  }
  const insertData = {
    summary: eventData.start.title,
    location: startLocation,
    start: {
      dateTime: DateTime.fromJSDate(eventData.start.date, { zone: 'utc' })
        .setZone(startTz, { keepLocalTime: true })
        .toISO(),
      timeZone: startTz,
    },
    end: {
      dateTime: DateTime.fromJSDate(eventData.end.date, { zone: 'utc' })
        .setZone(endTz, { keepLocalTime: true })
        .toISO(),
      timeZone: endTz,
    },
    extendedProperties: {
      private: {
        category: eventData.category,
        country: eventData.start.country,
        city: eventData.start.city,
      },
    },
    colorId: CALENDAR_COLORS[eventData.category],
  };

  if (startLocation !== endLocation) {
    insertData.extendedProperties.private.endLocation = endLocation;
    insertData.extendedProperties.private.endCountry = eventData.end.country;
    insertData.extendedProperties.private.endCity = eventData.end.city;
  }

  if (eventData.end.title) insertData.extendedProperties.private.endTitle = eventData.end.title;

  await doPost(GCAL_ENDPOINT, { auth: true, data: insertData });
};

const setLabels = async (threadId, newLabels, removeLabels) => {
  newLabels = newLabels.map((x) => labels.value[x]);
  removeLabels = removeLabels.map((x) => labels.value[x]);
  await doPost(`${GMAIL_THREADS_ENDPOINT}/${threadId}/modify`, {
    auth: true,
    data: {
      addLabelIds: newLabels,
      removeLabelIds: removeLabels,
    },
  });
};

const doFetch = async (method, url, opts) => {
  opts = opts || {};

  if (opts.content_type === undefined) opts.content_type = 'application/json';
  const headers = {};
  if (opts.content_type) headers['Content-Type'] = opts.content_type;
  if (opts.auth) headers.Authorization = `Bearer ${user.value.accessToken}`;

  const fetchOpts = {
    method,
    mode: 'cors',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers,
    redirect: 'follow',
    referrerPolicy: 'no-referrer',
  };

  const params = new URLSearchParams();
  if (opts.data) {
    for (const [key, value] of Object.entries(opts.data)) {
      params.append(key, value);
    }

    if (method === 'GET') {
      url += `?${params.toString()}`;
    } else if (method === 'POST') {
      fetchOpts.body = JSON.stringify(opts.data);
    }
  }

  const r = await fetch(url, fetchOpts);
  return r.json();
};

const doGet = (url, opts) => doFetch('GET', url, opts);
const doPost = (url, opts) => doFetch('POST', url, opts);
