import { DateTime } from 'luxon';
import { getAttachment } from './google';

const getFromHeaders = (header) => {
  header = header.toLowerCase();
  return (msg) => {
    for (const h of msg.payload.headers) {
      if (h.name.toLowerCase() === header) return h.value;
    }
  };
};

const makeText = (x) => atob(x.replace(/-/g, '+').replace(/_/g, '/'))

const getDateFromHeaders = getFromHeaders('Date');

export const getSubject = getFromHeaders('Subject');
export const getFrom = getFromHeaders('From');
export const getSender = getFromHeaders('Sender');
export const getDate = (msg) => {
  const d = getDateFromHeaders(msg);
  if (!d) return null;
  return DateTime.fromRFC2822(d);
};

export const getEmailAttachment = async (msg, mimeType) => {
  const attachmentId = getEmailPart(msg, mimeType, true);
  if (!attachmentId) return null;
  const attachment = await getAttachment(msg.id, attachmentId);
  return makeText(attachment.data);
}

export const getEmailPart = (msg, mimeType, attachment) => {
  if (!msg) return null;
  const checkParts = (data) => {
    let parts = data.parts;
    if (!parts) parts = [data];

    for (const part of parts) {
      if (part.mimeType === mimeType) {
        if (attachment) return part.body.attachmentId;
        return decodeURIComponent(escape(makeText(part.body.data)));
      }

      if (part.mimeType === 'multipart/related' || part.mimeType === 'multipart/mixed') {
        const subpart = checkParts(part);
        if (subpart) return subpart;
      }
    }
  };

  return checkParts(msg.payload);
};
