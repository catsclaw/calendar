import { createApp } from 'vue';
import App from './App.vue';
import router from './js/router';
import icon from './js/icons';
import ConfirmationService from 'primevue/confirmationservice';
import PrimeVue from 'primevue/config';

import 'v-calendar/style.css';

createApp(App).component('icon', icon).use(router).use(PrimeVue).use(ConfirmationService).mount('#app');
