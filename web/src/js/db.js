import { ref, computed } from 'vue';
import { DateTime } from 'luxon';
import { initializeApp } from 'firebase/app';
import { getAuth, onAuthStateChanged, GoogleAuthProvider, signInWithPopup } from 'firebase/auth';
import { getFirestore, connectFirestoreEmulator } from 'firebase/firestore';
import { getFunctions, httpsCallable, connectFunctionsEmulator } from 'firebase/functions';
import { handleLogin } from './users';

const emulateSetting = ref(localStorage.getItem('firebaseEmulation') || '1');
export const emulation = computed({
  get: () => process.env.NODE_ENV === 'development' && emulateSetting.value === '1',
  set: (val) => {
    const x = val ? '1' : '0';
    if (emulateSetting.value === val) return;
    localStorage.setItem('firebaseEmulation', x);
    location.reload();
  },
});

let firebaseApp = initializeApp({
  apiKey: 'AIzaSyAFzbiEF2bhCA7dYClwws9S4XlIxraHl9M',
  authDomain: 'calendar-1531256784619.firebaseapp.com',
  databaseURL: 'https://calendar-1531256784619.firebaseio.com',
  projectId: 'calendar-1531256784619',
  storageBucket: '',
  messagingSenderId: '933769757058',
});

const functions = getFunctions(firebaseApp);
if (emulation.value) connectFunctionsEmulator(functions, 'localhost', 5001);
export const recalcAllVisaInfo = httpsCallable(functions, 'recalcAllVisaInfo');
export const summarizeTravel = httpsCallable(functions, 'summarizeTravel');
export const summarizeEvents = httpsCallable(functions, 'summarizeEvents');
export const getScheduled = httpsCallable(functions, 'getScheduled');
export const getRoutes = httpsCallable(functions, 'getRoutes');
export const deleteEvent = httpsCallable(functions, 'deleteEvent');

export function toLuxon(d) {
  return DateTime.fromSeconds(d.seconds, { zone: 'utc' });
}

export function fromLuxon(d) {
  return d.toJSDate();
}

export const db = getFirestore();
if (emulation.value) connectFirestoreEmulator(db, 'localhost', 8080);

const auth = getAuth(firebaseApp);
// if (EMULATE) connectAuthEmulator(auth, 'http://localhost:9099');

onAuthStateChanged(auth, (user) => handleLogin(user));

export function login() {
  const provider = new GoogleAuthProvider();
  return signInWithPopup(auth, provider);
}

export function logout() {
  return auth.signOut();
}
