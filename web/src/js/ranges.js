import { ref, reactive, watch } from 'vue';
import { DateTime } from 'luxon';
import { db, toLuxon, fromLuxon } from './db';
import { calcBlocks, calcBlocksChunk } from '../js/util';
import {
  collection,
  query,
  where,
  onSnapshot,
  orderBy,
  getDocs,
  doc,
  deleteDoc,
  setDoc,
  addDoc,
} from 'firebase/firestore';
import { START_YEAR, END_YEAR, getZone, LEAVING_DATE } from './consts';
import { now } from './timer';
import { itemsLoaded } from './loaded';

export const ranges = reactive({});
export const rangesLoaded = ref(false);
export const searchFilter = ref(localStorage.getItem('searchFilter') || 'ongoing');

const rangeLoadRequested = ref(false);

const fetchCurrentRange = async () => {
  let blocks = calcBlocks(now.value, null, 'weeks');
  let q = query(
    collection(db, 'ranges'),
    orderBy('endDate'),
    where('blocks', 'array-contains-any', blocks),
    where('endDate', '>=', fromLuxon(now.value))
  );
  q = q.withConverter(rangeConverter);

  let ranges = null;
  let cr = null;
  let nr = null;
  try {
    ranges = await getDocs(q);
  } catch (err) {
    console.log(err);
  }

  if (ranges) {
    for (let r of ranges.docs) {
      r = r.data();
      if (cr) {
        if (+r.startDate === +cr.endDate || +r.startDate === +cr.endDate.plus({ days: 1 })) {
          nr = r;
          break;
        }
      }
      if (r.startDate <= now.value && r.endDate >= now.value) cr = r;
    }
  }

  if (cr && !nr) {
    blocks = calcBlocks(cr.endDate, cr.endDate.plus({ days: 1 }), 'weeks');
    let q = query(
      collection(db, 'ranges'),
      orderBy('startDate'),
      where('blocks', 'array-contains-any', blocks),
      where('startDate', '>=', fromLuxon(cr.endDate))
    );
    q = q.withConverter(rangeConverter);
    const ranges2 = await getDocs(q);
    for (let r of ranges2.docs) {
      r = r.data();
      if (+r.startDate === +cr.endDate || +r.startDate === +cr.endDate.plus({ days: 1 })) {
        nr = r;
        break;
      }
    }
  }

  currentRange.value = cr;
  nextRange.value = nr;
};

watch(searchFilter, (filter) => {
  localStorage.setItem('searchFilter', filter);
  setRangeSnapshot();
});

watch(now, fetchCurrentRange);

export const currentRange = ref(null);
export const nextRange = ref(null);

const rangeQueryUnsubs = [];

/*
  ARRANGED meanings:
  booked = I booked it
  required = I need to arrange it
  partial = I have booked some but not all
  unneeded = it does not need booking
  day-of = I can book it that day
  handled = Someone else took care of it
*/
const rangeConverter = {
  toFirestore: (range) => {
    return {
      area: range.area || null,
      arranged: range.arranged,
      blocks: calcBlocks(range.startDate, range.endDate),
      city: range.city,
      country: range.country,
      endDate: fromLuxon(range.endDate),
      host: range.host,
      latLng: range.latLng,
      note: range.type ? range.note : null,
      group: range.group,
      startDate: fromLuxon(range.startDate),
      tentative: range.tentative,
      type: range.type,
      zone: getZone(range.country, range.startDate),
    };
  },

  fromFirestore: (snapshot, options) => {
    const data = snapshot.data(options);
    const r = {
      _id: snapshot.id,
      area: data.area,
      arranged: data.arranged || {},
      blocks: data.blocks,
      city: data.city,
      country: data.country,
      endDate: toLuxon(data.endDate),
      host: data.host,
      latLng: data.latLng,
      note: data.note,
      group: data.group,
      startDate: toLuxon(data.startDate),
      tentative: data.tentative,
      type: data.type,
    };
    if (r.endDate < LEAVING_DATE) {
      r.arranged.room = r.arranged.room || 'booked';
    }
    r.arranged.room = r.arranged.room || 'required';
    delete r.arranged.hotel;
    return r;
  },
};

const unsub = (opts) => {
  rangeQueryUnsubs.map((u) => u());
  if (opts.clearAll) {
    opts.startDate = DateTime.utc(1900, 1, 1);
    opts.endDate = DateTime.utc(2100, 1, 1);
    itemsLoaded.ranges = false;
  }

  if (opts.startDate && opts.endDate) {
    for (let r of Object.values(ranges)) {
      if (r.endDate < opts.startDate || r.startDate > opts.endDate) delete ranges[r._id];
    }
  }
};

const setRangeSnapshot = () => {
  let startDate = DateTime.utc(START_YEAR, 1, 1);
  let endDate = DateTime.utc(END_YEAR, 12, 31);

  function generateQueries(startDate, endDate) {
    const qs = [];
    const blocks = calcBlocksChunk(startDate, endDate, 'quarters');
    for (let b of blocks) {
      qs.push(query(collection(db, 'ranges'), where('blocks', 'array-contains-any', b)));
    }
    return qs;
  }

  let queries = null;
  if (searchFilter.value[0] === 'y') {
    const year = parseInt(searchFilter.value.substring(1, 5));
    startDate = DateTime.utc(year, 1, 1);
    endDate = DateTime.utc(year, 12, 31);
    queries = generateQueries(startDate, endDate);
  } else {
    switch (searchFilter.value) {
      case 'all':
        queries = [collection(db, 'ranges')];
        break;
      case 'threemonth':
        startDate = now.value.minus({ months: 3 });
        endDate = now.value.plus({ months: 3 });
        queries = generateQueries(startDate, endDate);
        break;
      case 'ongoing':
        startDate = now.value.startOf('day');
        queries = generateQueries(startDate, endDate);
        break;
    }
  }

  unsub({ startDate, endDate });

  rangeQueryUnsubs.length = 0;
  for (let q of queries) {
    rangeQueryUnsubs.push(
      onSnapshot(
        q.withConverter(rangeConverter),
        (ss) => {
          ss.docChanges().forEach((change) => {
            rangesLoaded.value = true;

            if (change.type === 'removed') {
              delete ranges[change.doc.id];
              return;
            }
            const r = change.doc.data();
            if (r.endDate < startDate || r.startDate > endDate) return;
            if (change.type === 'added' || change.type === 'modified') {
              ranges[change.doc.id] = r;
            }
          });
        },
        (error) => {
          console.log(error);
        }
      )
    );
  }

  itemsLoaded.ranges = true;
};

const runLoad = () => {
  setRangeSnapshot();
  fetchCurrentRange();
};

export const loadRanges = () => {
  rangeLoadRequested.value = true;
  if (!itemsLoaded.ranges && itemsLoaded.user) runLoad();
  return ranges;
};

export const saveRange = async (r) => {
  if (r._id) {
    const ref = doc(db, 'ranges', r._id).withConverter(rangeConverter);
    await setDoc(ref, r);
  } else {
    const ref = collection(db, 'ranges').withConverter(rangeConverter);
    await addDoc(ref, r);
  }
};

export const deleteRange = async (r) => {
  const ref = doc(db, 'ranges', r._id);
  await deleteDoc(ref);
};

watch(itemsLoaded, () => {
  if (!itemsLoaded.user) unsub({ clearAll: true });
});
