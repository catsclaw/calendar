import { reactive } from 'vue';
import { db } from './db';
import { collection, onSnapshot, doc, getDoc, deleteDoc, setDoc } from 'firebase/firestore';
import { itemsLoaded } from './loaded';

export const profiles = reactive({});
export const user = reactive({
  user: null,
  profile: null,
  access: null,
  checkAccess: {
    admin: false,
    user: false,
    pending: false,
  },
});

let started = false;

const profileConverter = {
  toFirestore: (p) => {
    return {
      access: p.access,
      email: p.email,
      name: p.name,
    };
  },
  fromFirestore: (snapshot, options) => {
    const data = snapshot.data(options);
    return {
      _id: snapshot.id,
      access: data.access,
      email: data.email,
      name: data.name,
    };
  },
};

function startProfileSnapshot() {
  onSnapshot(
    collection(db, 'profiles').withConverter(profileConverter),
    (ss) => {
      ss.docChanges().forEach((change) => {
        if (change.type === 'removed') {
          delete profiles[change.doc.id];
        } else {
          const p = change.doc.data();
          if (change.type === 'added' || change.type === 'modified') {
            profiles[p._id] = p;
          }
        }
      });
      assignProfile();
    },
    (error) => {
      console.log(error);
    }
  );
}

async function getProfile(user) {
  const ref = doc(db, 'profiles', user.uid).withConverter(profileConverter);
  let profile = await getDoc(ref);
  if (profile.exists()) {
    profile = profile.data();
  } else {
    profile = {
      access: 'pending',
      email: user.email,
      name: user.displayName,
      _id: user.uid,
    };
    await saveProfile(profile);
  }
  profiles[ref.id] = profile;
  return profile;
}

export function loadProfiles() {
  if (!started) {
    startProfileSnapshot();
    started = true;
  }
  return profiles;
}

export async function saveProfile(p) {
  const ref = doc(db, 'profiles', p._id).withConverter(profileConverter);
  await setDoc(ref, p);
}

export async function deleteProfile(p) {
  const ref = doc(db, 'profiles', p._id);
  await deleteDoc(ref);
}

function assignProfile() {
  const u = user.user;
  if (!u) {
    user.profile = null;
    user.access = null;
    user.checkAccess.admin = false;
    user.checkAccess.user = false;
    user.checkAccess.pending = false;
    return;
  }
  const profile = profiles[u.uid] || {};
  const access = profile.access || null;
  user.profile = profile;
  user.access = access;
  user.checkAccess.admin = access === 'admin';
  user.checkAccess.user = access === 'admin' || access === 'user';
  user.checkAccess.pending = access === 'admin' || access === 'user' || access === 'pending';
}

export async function handleLogin(u) {
  user.user = u;
  if (u) {
    await getProfile(u);
    itemsLoaded.user = true;
  } else {
    user.user = null;
    itemsLoaded.user = false;
  }
  assignProfile();
}
