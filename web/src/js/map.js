let mapApi = null;
let loadingPromise = null;

let loadedTime = 0;
const RELOAD_TIME = 90; // In minutes

export function clearMapApi() {
  document.querySelectorAll('script[src^="https://maps.googleapis.com"]').forEach((script) => {
    script.remove();
  });
  if (window.google) delete window.google.maps;
  mapApi = null;
  loadedTime = 0;
}

export function attachAutocomplete(element, types, changeCallback) {
  const input = document.getElementById(element);
  if (!input) return;

  return loadMapApi().then((mapApi) => {
    const autocomplete = new mapApi.places.Autocomplete(input, {});
    autocomplete.setFields(['address_components', 'geometry', 'name']);
    if (changeCallback) {
      autocomplete.addListener('place_changed', () => changeCallback(autocomplete.getPlace()));
    }
    return autocomplete;
  });
}

export function loadMapApi() {
  if (loadingPromise) return loadingPromise;

  const currentTime = new Date().getTime();
  if (mapApi && currentTime < loadedTime + RELOAD_TIME * 60000) {
    return Promise.resolve(mapApi);
  }

  clearMapApi();

  const CALLBACK_NAME = '__googleMapsApiOnLoadCallback';

  const key = 'AIzaSyAee35za1S25nakAhIM_UifrBGm-q8XdAg';

  loadingPromise = new Promise((resolve, reject) => {
    // Reject the promise after a timeout
    const timeoutId = setTimeout(() => {
      window[CALLBACK_NAME] = () => {};
      reject(new Error('Could not load the Google Maps API'));
    }, 15000);

    // Hook up the on load callback
    window[CALLBACK_NAME] = () => {
      if (timeoutId !== null) {
        clearTimeout(timeoutId);
      }
      resolve(window.google.maps);
      window[CALLBACK_NAME] = () => {};
    };

    const scriptElement = document.createElement('script');
    scriptElement.src = `https://maps.googleapis.com/maps/api/js?callback=${CALLBACK_NAME}&key=${key}&language=en&libraries=places`;
    document.body.appendChild(scriptElement);
  }).then((api) => {
    mapApi = api;
    loadingPromise = null;
    loadedTime = new Date().getTime();
    return api;
  });

  return loadingPromise;
}
