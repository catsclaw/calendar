import { db, toLuxon, fromLuxon } from './db';
import { doc, getDoc, setDoc } from 'firebase/firestore';
import { getCountryCode } from './util';
import { DateTime } from 'luxon';
import { STATES } from './consts';

const countries = {};

const CALENDARIFIC_URL = 'https://calendarific.com/api/v2/holidays';
const CALENDARIFIC_API_KEY = 'd61eaf4f2ec0c1b8d45bc2a9dab86d31f09f2049';

let nextApiRequest = DateTime.utc();

const fetchApiResults = async (country, year) => {
  const r = await fetch(`${CALENDARIFIC_URL}?api_key=${CALENDARIFIC_API_KEY}&country=${country}&year=${year}`);
  if (!r.ok) throw new Error(`Bad response from API: ${r}`);
  const j = await r.json();
  return j.response.holidays;
};

const getKey = (country, year) => {
  return `${country}:${year}`;
};

const getCache = async (country, year) => {
  const key = getKey(country, year);
  const r = await getDoc(doc(db, 'holidays', key));
  if (!r.exists()) return null;
  const data = r.data();
  if (DateTime.utc() > toLuxon(data.expires)) return null;
  return data.data;
};

const storeCache = async (country, year, data) => {
  const key = getKey(country, year);
  const expires = fromLuxon(DateTime.utc().plus({ days: 30 }));
  return await setDoc(doc(db, 'holidays', key), {
    data,
    expires,
  });
};

const getCountryHolidays = async (country, year) => {
  let countryYear = countries[country];
  if (!countryYear) {
    countryYear = {};
    countries[country] = countryYear;
  }

  let yearData = countryYear[year];
  if (!yearData) {
    yearData = new Promise((resolve) => {
      getCache(country, year).then((cached) => {
        if (cached) {
          resolve(cached);
          return;
        }

        let current = DateTime.utc();
        let duration = nextApiRequest - current;
        if (duration < 0) duration = 0;
        current = current.plus({ seconds: 2 });
        nextApiRequest = current;
        setTimeout(() => {
          fetchApiResults(country, year).then((data) => {
            storeCache(country, year, data);
            resolve(data);
          }, duration);
        });
      });
    });
    countryYear[year] = yearData;
  }

  return yearData;
};

export function getHolidays(country, area, startDate, endDate) {
  const cc = getCountryCode(country);
  if (!cc) return [];

  const years = [];
  for (let y = startDate.year; y <= endDate.year; y++) {
    years.push(getCountryHolidays(cc, y.toString()));
  }

  return Promise.all(years).then((values) => {
    const holidays = [];
    let counter = 0;
    for (let h of values.flat()) {
      const holidayDate = DateTime.fromISO(h.date.iso, { zone: 'utc' });
      if (holidayDate < startDate || holidayDate > endDate) continue;
      if (h.type.includes('Worldwide observance')) continue;
      if (h.type.includes('United Nations observance')) continue;
      if (h.type.includes('Local observance')) continue;
      if (h.name !== 'Halloween' && h.type.includes('Observance')) continue;
      if (h.type.includes('Local holiday') && cc === 'US') {
        let foundState = false;
        for (let s of h.states) {
          if (s.abbrev === STATES[area]) {
            foundState = true;
            break;
          }
        }
        if (!foundState) continue;
      }
      holidays.push({
        id: counter++,
        name: h.name,
        date: holidayDate,
        description: h.description,
        type: h.type.join(', '),
      });
    }
    return holidays;
  });
}
