import { ref, reactive } from 'vue';
import { db } from '../js/db';
import { collection, onSnapshot, setDoc, addDoc, deleteDoc, doc } from 'firebase/firestore';
import { haversine } from '../js/util';

export const friends = reactive({});
let started = false;

export const friendDistance = ref(150);

export const friendSort = (a, b) => {
  if (a.distance !== b.distance) return a.distance - b.distance;
  const last = a.name.split(' ').at(-1).localeCompare(b.name.split(' ').at(-1));
  if (last !== 0) return last;
  return a.name.localeCompare(b.name);
};

export const friendsInArea = (pt, showAll) => {
  let friendList = Object.values(friends);
  if (pt) {
    friendList = friendList.map((f) => {
      return { ...f, distance: haversine(pt, f.latLng) };
    });
    if (!showAll) friendList = friendList.filter((f) => f.distance < friendDistance.value);
  }
  friendList.sort(friendSort);
  return friendList;
};

const friendConverter = {
  toFirestore: (f) => {
    const data = {
      name: f.name,
      city: f.city,
      country: f.country,
      latLng: f.latLng,
    };

    return data;
  },

  fromFirestore: (snapshot, options) => {
    const data = snapshot.data(options);
    const f = {
      _id: snapshot.id,
      name: data.name,
      city: data.city,
      country: data.country,
      latLng: data.latLng,
    };
    return f;
  },
};

async function startFriendsSnapshot() {
  onSnapshot(collection(db, 'friends').withConverter(friendConverter), (ss) => {
    ss.docChanges().forEach(
      (change) => {
        if (change.type === 'removed') {
          delete friends[change.doc.id];
          return;
        }
        const f = change.doc.data();
        if (change.type === 'added' || change.type === 'modified') {
          friends[f._id] = f;
        }
      },
      (error) => {
        console.log(error);
      }
    );
  });
}

export function loadFriends() {
  if (!started) {
    startFriendsSnapshot();
    started = true;
  }

  return friends;
}

loadFriends();

export async function saveFriend(f) {
  if (f._id) {
    const ref = doc(db, 'friends', f._id).withConverter(friendConverter);
    await setDoc(ref, f);
    return f._id;
  } else {
    const ref = collection(db, 'friends').withConverter(friendConverter);
    const docRef = await addDoc(ref, f);
    return docRef.id;
  }
}

export async function deleteFriend(f) {
  const ref = doc(db, 'friends', f._id);
  await deleteDoc(ref);
}
