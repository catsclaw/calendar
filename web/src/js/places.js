import { reactive, computed } from 'vue';
import { db, toLuxon, fromLuxon } from '../js/db';
import { DateTime } from 'luxon';
import { getSearchTerm } from '../js/util';
import { collection, onSnapshot, setDoc, addDoc, deleteDoc, doc } from 'firebase/firestore';

export const places = reactive({});
export const groups = reactive({});
let started = false;

const groupConverter = {
  toFirestore: (g) => {
    const data = {
      name: g.name,
      places: Array.from(g.places || []),
    };

    return data;
  },

  fromFirestore: (snapshot, options) => {
    const data = snapshot.data(options);
    const g = {
      _id: snapshot.id,
      name: data.name,
      places: new Set(data.places),
    };
    return g;
  },
};

const placeConverter = {
  toFirestore: (p) => {
    const data = {
      country: p.country,
      name: p.name,
      source: p.source,
    };

    if (p.startDate) data.startDate = fromLuxon(p.startDate);
    if (p.endDate) data.endDate = fromLuxon(p.endDate);
    if (p.dateRepeats) data.dateRepeats = p.dateRepeats;

    if (p.coords) data.coords = p.coords;
    if (p.bbox) data.bbox = p.bbox;
    if (p.provisional) data.provisional = p.provisional;

    if (p.visited) data.visited = p.visited === true ? true : fromLuxon(p.visited);
    if (p.defunct) data.defunct = true;

    return data;
  },

  fromFirestore: (snapshot, options) => {
    const data = snapshot.data(options);
    const p = {
      _id: snapshot.id,
      country: data.country,
      name: data.name,
      search: `${getSearchTerm(data.name)} ${getSearchTerm(data.country)}`,
      source: data.source,
    };

    if (data.coords) p.coords = data.coords;
    if (data.bbox) p.bbox = data.bbox;
    if (data.provisional) p.provisional = data.provisional;
    if (data.dateRepeats) p.dateRepeats = data.dateRepeats;

    const now = DateTime.utc();
    if (data.startDate) {
      p.startDate = toLuxon(data.startDate);
      p.endDate = toLuxon(data.endDate);

      let currentMonth = p.startDate.month;
      p.months = new Set();
      p.months.add(currentMonth);
      while (currentMonth !== p.endDate.month) {
        currentMonth += 1;
        if (currentMonth > 12) currentMonth = 1;
        p.months.add(currentMonth);
        if (p.months.size >= 12) break;
      }

      if (data.dateRepeats !== 'never') {
        p.startDate = p.startDate.set({ years: now.get('year') });
        if (p.startDate < now) p.startDate = p.startDate.plus({ years: 1 });
        while (p.endDate < p.startDate) {
          p.endDate = p.endDate.plus({ years: 1 });
        }
      }
    }

    if (data.visited) p.visited = data.visited === true ? true : toLuxon(data.visited);
    if (data.defunct) p.defunct = true;

    return p;
  },
};

const startPlaceSnapshot = async () => {
  onSnapshot(collection(db, 'places').withConverter(placeConverter), (ss) => {
    ss.docChanges().forEach(
      (change) => {
        if (change.type === 'removed') {
          delete places[change.doc.id];
          return;
        }
        const p = change.doc.data();
        if (change.type === 'added' || change.type === 'modified') {
          places[p._id] = p;
        }
      },
      (error) => {
        console.log(error);
      }
    );
  });

  onSnapshot(collection(db, 'placeGroups').withConverter(groupConverter), (ss) => {
    ss.docChanges().forEach(
      (change) => {
        if (change.type === 'removed') {
          delete groups[change.doc.id];
          return;
        }
        const g = change.doc.data();
        if (change.type === 'added' || change.type === 'modified') {
          groups[g._id] = g;
        }
      },
      (error) => {
        console.log(error);
      }
    );
  });
};

export const loadPlaces = () => {
  if (!started) {
    startPlaceSnapshot();
    started = true;
  }

  return { places, groups };
};

export const savePlace = async (p) => {
  if (p._id) {
    const ref = doc(db, 'places', p._id).withConverter(placeConverter);
    await setDoc(ref, p);
    return p._id;
  } else {
    const ref = collection(db, 'places').withConverter(placeConverter);
    const docRef = await addDoc(ref, p);
    return docRef.id;
  }
};

export const saveGroup = async (g) => {
  if (g._id) {
    const ref = doc(db, 'placeGroups', g._id).withConverter(groupConverter);
    await setDoc(ref, g);
    return g._id;
  } else {
    const ref = collection(db, 'placeGroups').withConverter(groupConverter);
    const docRef = await addDoc(ref, g);
    return docRef.id;
  }
};

export const deletePlace = async (p) => {
  const ref = doc(db, 'places', p._id);
  await deleteDoc(ref);
  const placeGroups = groupsByPlace.value[p._id];
  for (let g of placeGroups) {
    g.places = g.places.filter((x) => x._id !== p._id);
    await saveGroup(g);
  }
};

export const groupsByPlace = computed(() => {
  const placeToGroup = {};
  for (let g of Object.values(groups)) {
    let visited = false;
    for (let p of g.places) {
      if (!placeToGroup[p]) placeToGroup[p] = [];
      placeToGroup[p].push({ ...g });
      const p2 = places[p];
      visited = visited || (p2 && p2.visited);
    }

    if (visited) {
      for (let p of g.places) {
        placeToGroup[p].forEach((x) => {
          x.visited = true;
        });
      }
    }
  }
  return placeToGroup;
});
