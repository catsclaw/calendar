import { ref } from 'vue';
import { DateTime } from 'luxon';

export const now = ref(generateTime());

function generateTime() {
  return DateTime.local().startOf('day').setZone('utc', { keepLocalTime: true });
}

window.setInterval(() => {
  const newTime = generateTime();
  if (newTime.hasSame(now.value, 'day') && newTime.zoneName === now.value.zoneName) return;
  now.value = newTime;
}, 30 * 60 * 1000); // Half-hour
