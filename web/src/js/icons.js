import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

import {
  faArchway,
  faBan,
  faBed,
  faBus,
  faCar,
  faCompass,
  faExternalLinkAlt,
  faFlag,
  faFlagUsa,
  faGlobe,
  faGlobeAfrica,
  faGlobeAmericas,
  faGlobeAsia,
  faGlobeEurope,
  faHotel,
  faHourglassEnd,
  faHourglass,
  faHourglassStart,
  faLandmark,
  faLink,
  faMonument,
  faPassport,
  faPlane,
  faQuestionCircle,
  faQuidditch,
  faShip,
  faStar,
  faSync,
  faTicketAlt,
  faTrain,
  faUmbrellaBeach,
} from '@fortawesome/free-solid-svg-icons';

import { faCheckSquare, faEye, faEyeSlash, faSquare } from '@fortawesome/free-regular-svg-icons';

library.add(faArchway);
library.add(faBan);
library.add(faBed);
library.add(faBus);
library.add(faCar);
library.add(faCheckSquare);
library.add(faCompass);
library.add(faExternalLinkAlt);
library.add(faEye);
library.add(faEyeSlash);
library.add(faFlag);
library.add(faFlagUsa);
library.add(faGlobe);
library.add(faGlobeAfrica);
library.add(faGlobeAmericas);
library.add(faGlobeAsia);
library.add(faGlobeEurope);
library.add(faHotel);
library.add(faHourglass);
library.add(faHourglassStart);
library.add(faHourglassEnd);
library.add(faLandmark);
library.add(faLink);
library.add(faMonument);
library.add(faPlane);
library.add(faQuestionCircle);
library.add(faQuidditch);
library.add(faShip);
library.add(faSquare);
library.add(faStar);
library.add(faSync);
library.add(faTicketAlt);
library.add(faTrain);
library.add(faUmbrellaBeach);
library.add(faPassport);

export default FontAwesomeIcon;
