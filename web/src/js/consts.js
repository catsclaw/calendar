import { DateTime } from 'luxon';

export const LEAVING_DATE = DateTime.utc(2018, 8, 13);

export const START_YEAR = 2015;
export const END_YEAR = DateTime.now().year + 3;

export const ARRANGED_ICONS = {
  room: 'bed',
  flight: 'plane',
  bus: 'bus',
  car: 'car',
  train: 'train',
  ship: 'ship',
};

/* ** REMEMBER **
   Update this in the backend as well */
export const ZONES = {
  'Åland Islands': 'Schengen',
  Aruba: 'NL-K',
  Austria: 'Schengen',
  Belgium: 'Schengen',
  Bonaire: 'NL-K',
  Croatia: 'Schengen',
  Cuba: 'United States',
  Curaçao: 'NL-K',
  Czechia: 'Schengen',
  Denmark: 'Schengen',
  'El Salvador': 'CA-4',
  Estonia: 'Schengen',
  Finland: 'Schengen',
  France: 'Schengen',
  Germany: 'Schengen',
  Greece: 'Schengen',
  Guatemala: 'CA-4',
  Honduras: 'CA-4',
  Hungary: 'Schengen',
  Iceland: 'Schengen',
  Italy: 'Schengen',
  Israel: 'Israel+',
  Latvia: 'Schengen',
  Liechtenstein: 'Schengen',
  Lithuania: 'Schengen',
  Luxembourg: 'Schengen',
  Malta: 'Schengen',
  Netherlands: 'Schengen',
  Nicaragua: 'CA-4',
  Norway: 'Schengen',
  Palestine: 'Israel+',
  Poland: 'Schengen',
  Portugal: 'Schengen',
  Slovakia: 'Schengen',
  Slovenia: 'Schengen',
  Spain: 'Schengen',
  Sweden: 'Schengen',
  Switzerland: 'Schengen',
};

export const getZone = (country, date) => {
  if (country == 'Portugal' && date > DateTime.utc(2024, 10, 3)) return 'Portugal';
  if (country == 'Croatia' && date < DateTime.utc(2023, 1, 1)) return 'Croatia';
  if (country == 'Romania' && date < DateTime.utc(2024, 3, 31)) return 'Romania';
  if (country == 'Bulgaria' && date < DateTime.utc(2024, 3, 31)) return 'Bulgaria';
  return ZONES[country] || country;
};

export const STATES = {
  Alabama: 'AL',
  Alaska: 'AK',
  Arizona: 'AZ',
  Arkansas: 'AR',
  California: 'CA',
  Colorado: 'CO',
  Connecticut: 'CT',
  Delaware: 'DE',
  Florida: 'FL',
  Georgia: 'GA',
  Hawaii: 'HI',
  Idaho: 'ID',
  Illinois: 'IL',
  Indiana: 'IN',
  Iowa: 'IA',
  Kansas: 'KS',
  Kentucky: 'KY',
  Louisiana: 'LA',
  Maine: 'ME',
  Maryland: 'MD',
  Massachusetts: 'MA',
  Michigan: 'MI',
  Minnesota: 'MN',
  Mississippi: 'MS',
  Missouri: 'MO',
  Montana: 'MT',
  Nebraska: 'NE',
  Nevada: 'NV',
  'New Hampshire': 'NH',
  'New Jersey': 'NJ',
  'New Mexico': 'NM',
  'New York': 'NY',
  'North Carolina': 'NC',
  'North Dakota': 'ND',
  Ohio: 'OH',
  Oklahoma: 'OK',
  Oregon: 'OR',
  Pennsylvania: 'PA',
  'Rhode Island': 'RI',
  'South Carolina': 'SC',
  'South Dakota': 'SD',
  Tennessee: 'TN',
  Texas: 'TX',
  Utah: 'UT',
  Vermont: 'VT',
  Virginia: 'VA',
  Washington: 'WA',
  'West Virginia': 'WV',
  Wisconsin: 'WI',
  Wyoming: 'WY',
  'American Samoa': 'AS',
  'District of Columbia': 'DC',
  'Federated States of Micronesia': 'FM',
  Guam: 'GU',
  'Marshall Islands': 'MH',
  'Northern Mariana Islands': 'MP',
  Palau: 'PW',
  'Puerto Rico': 'PR',
  'Virgin Islands': 'VI',
};
