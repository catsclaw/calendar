export const breakApartGeocode = (p) => {
  const results = {
    city: p.name,
    country: null,
    area: null,
  };

  let locality = null;

  for (const a of p.address_components) {
    if (a.types.includes('administrative_area_level_1')) {
      results.area = a.long_name;
    }

    if (a.types.includes('country')) {
      results.country = a.long_name;
    }

    if (a.types.includes('locality')) {
      locality = a;
    }
  }

  // Check for exceptions
  if (!results.country && locality) {
    if (locality.long_name === 'Prishtina' || locality.long_name === 'Pristina') {
      results.country = 'Kosovo';
    } else if (locality.long_name === 'Bethlehem' || locality.long_name === 'Ramallah') {
      results.country = 'Palestine';
    }
  }

  return results;
};
