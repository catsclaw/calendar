import { createWebHistory, createRouter } from 'vue-router';

import BannerView from '../views/BannerView.vue';

const routes = [
  {
    path: '/',
    component: BannerView,
    children: [
      {
        path: '',
        name: 'ranges',
        component: () => import('../views/RangesView.vue'),
      },
      {
        path: 'events',
        name: 'events',
        component: () => import('../views/EventView.vue'),
      },
      {
        path: 'tabs',
        name: 'tabs',
        component: () => import('../views/TabsView.vue'),
        // }, {
        //   path: 'map',
        //   name: 'map',
        //   component: () => import('../views/MapView.vue'),
      },
      {
        path: 'places',
        name: 'places',
        component: () => import('../views/PlaceView.vue'),
      },
      {
        path: 'friends',
        name: 'friends',
        component: () => import('../views/FriendsView.vue'),
        // }, {
        //   path: 'users',
        //   name: 'users',
        //   component: () => import('../views/UsersView.vue'),
      },
    ],
  },
  {
    path: '/hero',
    name: 'hero',
    component: () => import('../views/HeroView.vue'),
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
