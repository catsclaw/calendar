import iso from 'iso-3166-1';

const REPL = {
  å: 'a',
  ã: 'a',
  à: 'a',
  á: 'a',
  ä: 'a',
  â: 'a',
  ẽ: 'e',
  è: 'e',
  é: 'e',
  ë: 'e',
  ê: 'e',
  ì: 'i',
  í: 'i',
  ï: 'i',
  î: 'i',
  õ: 'o',
  ò: 'o',
  ó: 'o',
  ö: 'o',
  ô: 'o',
  ù: 'u',
  ú: 'u',
  ü: 'u',
  û: 'u',
  ñ: 'n',
  ç: 'c',
  ş: 's',
};

export function getFlagGraphic(country) {
  let flag = country.toLowerCase().replaceAll(' ', '-');
  for (let x in REPL) flag = flag.replaceAll(x, REPL[x]);
  return `/flags/${flag}.svg`;
}

export function calcBlocks(startDate, endDate, precision) {
  const blocks = new Set();
  let blockDate = startDate;
  if (!endDate) endDate = startDate;
  while (blockDate <= endDate) {
    if (!precision || precision === 'quarters') blocks.add('Q' + blockDate.toFormat('y-q'));
    if (!precision || precision === 'weeks') blocks.add('W' + blockDate.toFormat('kkkk-W'));
    blockDate = blockDate.plus({ days: 1 });
  }
  return Array.from(blocks);
}

export function getTypeIcon(type) {
  if (type === 'Larp') return 'quidditch';
  if (type === 'Convention') return 'hotel';
  if (type === 'Event') return 'ticket-alt';
  if (type === 'Vacation') return 'umbrella-beach';
}

export function getTravelIconClass(icon) {
  switch (icon) {
    case 'day-of':
      return 'has-text-grey-dark';
    case 'handled':
      return 'has-text-success';
    case 'required':
      return 'has-text-danger';
    case 'booked':
      return 'has-text-info';
    case 'partial':
      return 'has-text-warning';
    case 'unneeded':
    default:
      return 'has-text-grey-light';
  }
}

export function getSearchTerm(search) {
  if (search === null) return null;
  let s = search.toLowerCase();
  for (let x in REPL) s = s.replaceAll(x, REPL[x]);
  s = s.replace(/[^a-z0-9]/g, '');
  s = s.replace(/\s+/g, ' ');
  s = s.trim();
  return s;
}

export function calcBlocksChunk(startDate, endDate, precision) {
  const blocks = calcBlocks(startDate, endDate, precision);
  const res = [];
  for (let i = 0; i < blocks.length; i += 10) {
    const chunk = blocks.slice(i, i + 10);
    res.push(chunk);
  }
  return res;
}

export function th(d) {
  let s = d.toFormat('d');
  switch (s) {
    case '1':
    case '21':
    case '31':
      return s + 'st';
    case '2':
    case '22':
      return s + 'nd';
    case '3':
    case '23':
      return s + 'rd';
    default:
      return s + 'th';
  }
}

const overrideCodes = {
  'The Bahamas': 'BA',
  'Falkland Islands (Islas Malvinas)': 'FK',
  'North Macedonia': 'MK',
  'United States': 'US',
  'United Kingdom': 'UK',
  'Czechia': 'CZ',
};

export function getCountryCode(country) {
  if (overrideCodes[country]) return overrideCodes[country];
  const data = iso.whereCountry(country);
  if (data) return data.alpha2;
  console.log(`Unable to look up country code for ${country}`);
  return null;
}

export function dateDisplay(start, end) {
  let dateString = start.toFormat('MMM ') + th(start);
  if (start.equals(end)) {
    dateString += start.toFormat(' yyyy');
  } else {
    if (start.year !== end.year) {
      dateString += start.toFormat(' yyyy-') + end.toFormat('MMM ') + th(end) + end.toFormat(', yyyy');
    } else {
      if (start.month !== end.month) {
        dateString += end.toFormat('-MMM ') + th(end) + end.toFormat(', yyyy');
      } else {
        dateString += '-' + th(end) + end.toFormat(', yyyy');
      }
    }
  }

  return dateString;
}

export function sortStartDates(a, b) {
  let sortOrder = a.startDate - b.startDate;
  if (sortOrder === 0) sortOrder = a.endDate - b.endDate;
  return sortOrder;
}

export function copyToClipboard(text) {
  const textArea = document.createElement('textarea');

  textArea.style.position = 'fixed';
  textArea.style.top = 0;
  textArea.style.left = 0;
  textArea.style.width = '2em';
  textArea.style.height = '2em';
  textArea.style.padding = 0;
  textArea.style.border = 'none';
  textArea.style.outline = 'none';
  textArea.style.boxShadow = 'none';
  textArea.style.background = 'transparent';
  textArea.value = text;

  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();

  document.execCommand('copy');

  document.body.removeChild(textArea);
}

export const haversine = (p1, p2) => {
  function toRad(x) {
    return (x * Math.PI) / 180;
  }

  const lon1 = p1.lng || p1._long;
  const lat1 = p1.lat || p1._lat;

  const lon2 = p2.lng || p2._long;
  const lat2 = p2.lat || p2._lat;

  const R = 6371; // km

  const x1 = lat2 - lat1;
  const dLat = toRad(x1);
  const x2 = lon2 - lon1;
  const dLon = toRad(x2);
  const a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  const d = R * c;

  return d;
};
