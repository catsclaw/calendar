'use strict';

const admin = require('firebase-admin');
const functions = require('firebase-functions');
const cors = require('cors')({ origin: true });

const { getScheduled, deleteEvent } = require('./calendar');
const { toLuxon, getRanges, calculateAvailableDays, setCalculations, toLuxonISO, correctZones } = require('./visas');
const { generateTravelDays, summarizeTravel, summarizeEvents, getRoutes, currentRoutes } = require('./calc');
const { DateTime } = require('luxon');
const ical = require('ical-generator');

const SUPER_SECRET_KEY = 'a#D*J88D*DJ#d38d8OQhdw8fDJ#DJ#OJD(#samkd';

admin.initializeApp();
const db = admin.firestore();

exports.ping = functions.https.onRequest((req, res) => {
  return cors(req, res, () => res.json({ status: 'ok' }));
});

exports.maintainVisaInfo = functions.firestore.document('ranges/{rangeID}').onWrite(async (change, context) => {
  const before = change.before.exists ? change.before.data() : null;
  const after = change.after.exists ? change.after.data() : null;

  if (after && before && before.startDate === after.startDate && before.endDate === after.endDate && before.zone === after.zone) return;

  // Invalidate the summary caches
  const tabCaches = new Set(['all', '2018-08-14:present', '2000-01-01:present']);
  if (before) {
    tabCaches.add(`y${toLuxon(before.startDate).year}`);
    tabCaches.add(`y${toLuxon(before.endDate).year}`);
  }

  if (after) {
    tabCaches.add(`y${toLuxon(after.startDate).year}`);
    tabCaches.add(`y${toLuxon(after.endDate).year}`);
  }

  for (const id of tabCaches) {
    db.collection('tabCacheInvalidate').doc(id).set({
      invalid: true,
    });
  }

  let affectedZones = new Set();
  let earliestStart = null;

  if (before) {
    affectedZones.add(before.zone);
    earliestStart = before.startDate;
  }

  if (after) {
    affectedZones.add(after.zone);
    if (!earliestStart || earliestStart > after.startDate) earliestStart = after.startDate;
  }

  earliestStart = toLuxon(earliestStart);

  await db.runTransaction(async t => {
    await recalcVisaInfo(earliestStart, affectedZones, t);
  });
});

async function recalcVisaInfo(startDate, zones, t) {
  const { ranges, calculations } = await getRanges(startDate, zones, db, t);
  const travelDays = generateTravelDays(ranges);
  const available = calculateAvailableDays(ranges, travelDays);
  const changedCalcs = {};
  for (let [id, a] of Object.entries(available)) {
    const c = calculations[id];
    delete calculations[id];
    if (!c || c.start !== a.start || c.end !== a.end || c.info !== a.info) {
      changedCalcs[id] = a;
    }
  }
  await setCalculations(changedCalcs, Object.keys(calculations), db, t);
}

exports.recalcAllVisaInfo = functions.https.onCall(async (data, context) => {
  const startDate = toLuxonISO('2000-01-01');
  await correctZones(db);
  await recalcVisaInfo(startDate, null, null);
  return {};
});

exports.summarizeTravel = functions.https.onCall(async (data, context) => {
  let startDate, endDate = null;
  const key = data.key;

  const cache = await db.collection('tabCacheInvalidate').doc(key).get();
  if ((cache.data() || {}).invalid === false) return { 'status': 'ok '};

  if (key === 'all') {
    startDate = DateTime.utc(2000, 1, 1);
    endDate = DateTime.utc(2100, 1, 1);
  } else if (key === '2000-01-01:present') {
    startDate = DateTime.utc(2000, 1, 1);
    endDate = DateTime.utc();
  } else if (key === '2018-08-14:present') {
    startDate = DateTime.utc(2018, 8, 14);
    endDate = DateTime.utc();
  } else if (key.substring(0, 1) === 'y') {
    const year = parseInt(key.substring(1));
    startDate = DateTime.utc(year, 1, 1);
    endDate = DateTime.utc(year, 12, 31);
  } else {
    return { 'status': 'error' };
  }

  const { ranges } = await getRanges(startDate, null, db);
  const results = await summarizeTravel(ranges, startDate, endDate);
  await db.collection('tabCache').doc(key).set({ results });
  await db.collection('tabCacheInvalidate').doc(key).set({ invalid: false });
  return { 'status': 'ok' };
});

exports.summarizeEvents = functions.https.onCall(async (data, context) => {
  const startDate = toLuxonISO('2000-01-01');
  const { ranges } = await getRanges(startDate, null, db);
  return await summarizeEvents(ranges);
});

exports.getRoutes = functions.https.onCall(async (data, context) => {
  const startDate = toLuxonISO('2000-01-01');
  const { ranges } = await getRanges(startDate, null, db);
  return await getRoutes(ranges);
});

exports.getScheduled = functions.https.onCall(async (data, context) => {
  return await getScheduled(data);
});

exports.deleteEvent = functions.https.onCall(async (data, context) => {
  return await deleteEvent(data.id);
});

exports.generateICal = functions.https.onRequest(async (req, res) => {
  let startDate = null;
  let endDate = null;
  let name = null;
  let timezone = req.query.tz || 'America/New_York';
  if (req.query.key === 'qa9U5xxUf1VTNNQ') {
    startDate = toLuxonISO('2022-01-11');
    endDate = toLuxonISO('2022-03-17');
    name = 'Mexico and South America with Daria';
    timezone = 'Europe/Moscow';
  } else if (req.query.key === 'XK4sx75x8xk5OFM') {
    startDate = DateTime.now().minus({ weeks: 1 });
    endDate = DateTime.now().plus({ weeks: 4 })
    name = 'Ongoing travel';
  } else {
    return res.status(404).json({ error: 'Unknown calendar' });
  }

  const calendar = ical({ name });
  const query = db.collection('ranges').where('endDate', '>=', startDate.toJSDate()).where('endDate', '<=', endDate.toJSDate());
  const docs = await query.get();
  docs.forEach(d => {
    const data = d.data();
    calendar.createEvent({
      start: toLuxon(data.startDate).setZone(timezone).set({ hour: 12, minute: 0, seconds: 0 }),
      end: toLuxon(data.endDate).setZone(timezone).set({ hour: 12, minute: 0, seconds: 0 }),
      summary: data.city + ', ' + data.country,
      location: data.city + ', ' + data.country,
    });
  });

  return calendar.serve(res);
});
