const { toLuxon } = require('./visas');

function generateTravelDays(ranges) {
  const zones = {};

  Object.values(ranges).forEach(r => {
    if (!zones[r.zone]) zones[r.zone] = new Set();
    if (!zones[r.country]) zones[r.country] = new Set();
    let date = toLuxon(r.startDate);
    const endDate = toLuxon(r.endDate);
    while (date <= endDate) {
      const iso = date.toISODate();
      zones[r.country].add(iso);
      zones[r.zone].add(iso);
      date = date.plus({ days: 1 });
    }
  });

  return zones;
}

exports.generateTravelDays = generateTravelDays;

function summarizeTravel(ranges, startDate, endDate) {
  const present = {};
  const summary = {};

  Object.values(ranges).forEach(r => {
    const area = r.area || '—'
    const areaKey = `${r.country}|${area}`;
    const cityKey = `${r.country}|${area}|${r.city}`;
    let date = toLuxon(r.startDate);
    const rangeEndDate = toLuxon(r.endDate);
    if (date > endDate || rangeEndDate < startDate) return;
    while (date <= rangeEndDate) {
      const iso = date.toISODate();
      if (!present[iso]) present[iso] = new Set();
      present[iso].add(r.country);
      present[iso].add(areaKey);
      present[iso].add(cityKey);
      if (!summary[r.country]) summary[r.country] = {
        areas: {},
        visits: 0,
        days: 0,
      }
      if (!summary[r.country].areas[area]) summary[r.country].areas[area] = {
        cities: {},
        visits: 0,
        days: 0,
      }
       if (!summary[r.country].areas[area].cities[r.city]) summary[r.country].areas[area].cities[r.city] = {
        visits: 0,
        days: 0,
      }
      date = date.plus({ days: 1 });
    }
  });

  date = startDate;
  let active = new Set();

  function markSummary(newActive) {
    for (let a of newActive) {
      const split = a.split('|');
      if (split.length === 3) {
        let [country, area, city] = split;
        summary[country].areas[area].cities[city].days += 1;
        if (!active.has(a)) summary[country].areas[area].cities[city].visits += 1;
      } else if (split.length === 2) {
        let [country, area] = split;
        summary[country].areas[area].days += 1;
        if (!active.has(a)) summary[country].areas[area].visits += 1;
      } else if (split.length === 1) {
        let [country] = split;
        summary[country].days += 1;
        if (!active.has(a)) summary[country].visits += 1;
      }
    }
    active = newActive;
  }

  while (date <= endDate) {
    const iso = date.toISODate();
    date = date.plus({ days: 1 });
    const newAreas = present[iso] || new Set();
    markSummary(newAreas);
  }

  markSummary(new Set());
  return summary;
}

exports.summarizeTravel = summarizeTravel;

function summarizeEvents(ranges) {
  const notes = {};
  ranges = Object.entries(ranges);
  ranges.sort((a, b) => b[1].startDate.seconds - a[1].startDate.seconds);
  ranges.forEach(([id, r]) => {
    if (!r.note) return;
    const event = {
      id,
      name: r.note,
      type: r.type,
      tentative: r.tentative,
      city: r.city,
      startDate: r.startDate,
      endDate: r.endDate,
    }

    if (r.group) id = r.group;
    if (!notes[id]) notes[id] = { events: [] };
    notes[id].events.push(event);
  });

  Object.entries(notes).forEach(([id, e]) => {
    e.id = id;
    if (e.events.length === 1) {
      e.name = e.events[0].name;
    } else {
      e.name = id;
    }
    e.types = Array.from(new Set(e.events.map(x => x.type)));
    e.cities = Array.from(new Set(e.events.map(x => x.city)));
    e.startDate = new Date(Math.min(...e.events.map(x => x.startDate.toMillis()))).toISOString();
    e.endDate = new Date(Math.max(...e.events.map(x => x.endDate.toMillis()))).toISOString();
    e.tentative = e.events.reduce((p, c) => c.tentative && p, true);
  });

  return Object.values(notes);
}

exports.summarizeEvents = summarizeEvents;

function getRoutes(ranges) {
  const rangeByStartDate = {}
  ranges = Object.values(ranges).sort((a, b) => b.seconds - a.seconds);
  ranges.forEach(r => {
    const iso = toLuxon(r.startDate).toISODate();
    let s = rangeByStartDate[iso] || [];
    s.push(r);
    rangeByStartDate[iso] = s;
  });
  const routes = [];
  ranges.forEach(r => {
    const endDate = toLuxon(r.endDate);
    let nextStops = rangeByStartDate[endDate.toISODate()];
    if (!nextStops) nextStops = rangeByStartDate[endDate.plus({ days: 1 }).toISODate()];
    if (!nextStops) return;
    let nextStop = null;
    let sameDay = null;
    for (let next of nextStops) {
      if (next.country === r.country && next.city === r.city && next.area === r.area) continue;
      if (next.startDate === next.endDate) {
        sameDay = next;
        continue;
      }
      nextStop = next;
      break;
    }
    if (!nextStop) nextStop = sameDay;
    if (!nextStop) return;
    routes.push({
      startCountry: r.country,
      endCountry: nextStop.country,
      startArea: r.area,
      endArea: nextStop.area,
      startCity: r.city,
      endCity: nextStop.city,
      startLatLng: r.latLng,
      endLatLng: nextStop.latLng,
      date: endDate.toISODate(),
      startTentative: r.tentative,
      endTentative: nextStop.tentative,
    });
  });
  return routes;
}

exports.getRoutes = getRoutes;
