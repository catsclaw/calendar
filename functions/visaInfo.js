const { DateTime, Duration } = require('luxon');

// TYPES are:
//   visa - visa required
//   duration - can stay for X time after arrival
//   period - X days in a Y period

const VISA_INFO = {
  'Afghanistan': {
    type: 'visa',
  },
  'Albania': {
    type: 'duration',
    period: '1Y',
  },
  'Andorra': {
    type: 'duration',
    period: '3M',
  },
  'Algeria': {
    type: 'visa',
  },
  'Angola': {
    type: 'duration',
    evisa: true,
    period: '30D',
  },
  'Antigua and Barbuda': {
    type: 'duration',
    period: '6M',
  },
  'Argentina': {
    type: 'duration',
    period: '90D',
    extension: '180D',
  },
  'Armenia': {
    type: 'duration',
    period: '180D',
  },
  'Australia': {
    type: 'duration',
    evisa: true,
    period: '90D',
  },
  'Azerbaijan': {
    type: 'duration',
    period: '30D',
    info: 'Must register if staying more than ten days',
    voa: true,
    evisa: true,
  },
  'The Bahamas': {
    type: 'duration',
    period: '8M',
  },
  'Bahrain': {
    type: 'duration',
    period: '14D',
    voa: true,
    evisa: true,
  },
  'Bangladesh': {
    type: 'duration',
    period: '30D',
    info: 'Available at limited airports',
    voa: true,
  },
  'Barbados': {
    type: 'duration',
    period: '6M',
  },
  'Belarus': {
    type: 'duration',
    period: '30D',
    info: 'Must arrive and depart from Minsk\nTravel to or from Russia requires a visa',
    registration: '0D',
  },
  'Belize': {
    type: 'duration',
    period: '90D',
  },
  'Benin': {
    type: 'duration',
    evisa: true,
    period: '90D',
  },
  'Bhutan': {
    type: 'visa',
  },
  'Bolivia': {
    type: 'duration',
    period: '30D',
    voa: true,
  },
  'Bosnia and Herzegovina': {
    type: 'period',
    days: 30,
    period: '90D',
    registration: '24H',
  },
  'Botswana': {
    type: 'period',
    days: 90,
    period: '1Y',
  },
  'Brazil': { // MORE COMPLICATED
    type: 'period',
    days: 180,
    period: '1Y',
  },
  'Brunei': {
    type: 'duration',
    period: '90D',
  },
  'Bulgaria': {
    type: 'duration',
    period: '90D',
  },
  'Burkina Faso': { // ENTER VISA INFO
    type: 'visa',
  },
  'Burundi': {
    type: 'visa',
  },
  'Cambodia': {
    type: 'duration',
    period: '30D',
    voa: true,
    evisa: true,
  },
  'Cameroon': {
    type: 'visa',
  },
  'Canada': {
    type: 'duration',
    period: '6M',
  },
  'Cape Verde': {
    type: 'duration',
    period: '30D',
    info: 'Must register online at least five days prior to arrival',
  },
  'Central African Republic': {
    type: 'duration',
    period: '180D',
  },
  'Chad': {
    type: 'visa',
  },
  'Chile': {
    type: 'duration',
    period: '90D',
  },
  'China': {
    type: 'visa',
    registration: '24H',
  },
  'Colombia': {
    type: 'duration',
    period: '90D',
    extension: '180D',
  },
  'Comoros': {
    type: 'duration',
    period: '45D',
    voa: true,
  },
  'Democratic Republic of the Congo': {
    type: 'visa',
    registration: '0D',
  },
  'Republic of the Congo': {
    type: 'visa',
  },
  'Costa Rica': {
    type: 'duration',
    period: '90D',
  },
  'Côte d\'Ivoire': {
    type: 'duration',
    evisa: true,
    period: '3M',
  },
  // Pre-2023
  'Croatia': {
    type: 'period',
    days: 90,
    period: '180D',
    registration: '24H',
  },
  'Cuba': {
    type: 'visa',
  },
  'Cyprus': {
    type: 'period',
    days: 90,
    period: '180D',
  },
  // Denmark has additional info
  'Djibouti': {
    type: 'duration',
    evisa: true,
    period: '31D',
  },
  'Dominica': {
    type: 'duration',
    period: '6M',
  },
  'Dominican Republic': {
    type: 'duration',
    period: '30D',
    extension: '90D',
  },
  'Ecuador': {
    type: 'duration',
    period: '90D',
    extension: '180D',
  },
  'Egypt': {
    type: 'duration',
    evisa: true,
    period: '30D',
    voa: true,
    evisa: true,
  },
  'Equatorial Guinea': {
    type: 'duration',
    period: '90D',
  },
  'Eritrea': {
    type: 'visa',
  },
  'Eswatini': {
    type: 'duration',
    period: '30D',
    extension: '60D',
  },
  'Ethiopia': {
    type: 'duration',
    period: '90D',
    info: 'Must arrive at Addis Ababa Bole International Airport',
  },
  'Fiji': {
    type: 'duration',
    period: '4M',
  },
  // France has additional info
  'Gambia': {
    type: 'visa',
  },
  'Georgia': {
    type: 'duration',
    period: '1Y',
  },
  'Ghana': {
    type: 'visa',
  },
  'Grenada': {
    type: 'duration',
    period: '3M',
  },
  'Guinea': {
    type: 'duration',
    evisa: true,
    period: '5Y',
  },
  'Guinea-Bissau': {
    type: 'duration',
    period: '90D',
    voa: true,
    evisa: true,
  },
  'Guyana': {
    type: 'duration',
    period: '3M',
  },
  'Haiti': {
    type: 'duration',
    period: '3M',
  },
  'India': {
    type: 'duration',
    evisa: true,
    period: '180D',
  },
  'Indonesia': {
    type: 'duration',
    period: '30D',
  },
  'Iran': {
    type: 'visa',
  },
  'Iraq': {
    type: 'duration',
    period: '60D',
    voa: true,
  },
  'Ireland': { // HANDLE COVID
    type: 'duration',
    period: '3M',
  },
  'Jamaica': {
    type: 'duration',
    period: '6M',
  },
  'Japan': {
    type: 'duration',
    period: '90D',
  },
  'Jordan': {
    type: 'duration',
    period: '30D',
    voa: true,
  },
  'Kazakhstan': {
    type: 'duration',
    period: '30D',
  },
  'Kenya': {
    type: 'duration',
    evisa: true,
    period: '3M',
  },
  'Kiribati': {
    type: 'duration',
    period: '30D',
  },
  'North Korea': {
    type: 'visa',
  },
  'South Korea': {
    type: 'duration',
    period: '90D',
    info: 'Requires K-ETA',
  },
  'Kosovo': {
    type: 'period',
    days: 90,
    period: '6M',
  },
  'Kuwait': {
    type: 'duration',
    period: '3M',
    voa: true,
    evisa: true,
  },
  'Kyrgyzstan': {
    type: 'duration',
    period: '60D',
  },
  'Laos': {
    type: 'duration',
    period: '30D',
    voa: true,
    evisa: true,
  },
  // Latvia has additional info
  'Lebanon': {
    type: 'duration',
    period: '1M',
    extension: '3M',
  },
  'Lesotho': {
    type: 'duration',
    period: '14D',
    extension: '180D',
  },
  'Liberia': {
    type: 'visa',
  },
  'Libya': {
    type: 'visa',
  },
  'Madagascar': {
    type: 'duration',
    period: '3M',
    voa: true,
    evisa: true,
  },
  'Malawi': { // GET INFO
    type: 'duration',
    voa: true,
    evisa: true,
  },
  'Malaysia': {
    type: 'duration',
    period: '3M',
  },
  'Maldives': {
    type: 'duration',
    voa: true,
    period: '90D',
    extension: '90D',
  },
  'Mali': {
    type: 'visa',
  },
  'Marshall Islands': {
    type: 'unlimited',
  },
  'Mauritania': { // HOW LONG
    type: 'duration',
    voa: true,
  },
  'Mauritius': {
    type: 'duration',
    period: '90D',
  },
  'Mexico': {
    type: 'duration',
    period: '180D',
  },
  'Micronesia': {
    type: 'unlimited',
  },
  'Moldova': {
    type: 'period',
    days: 90,
    period: '180D',
    registration: '0D',
  },
  // Check Monaco
  'Mongolia': {
    type: 'duration',
    period: '90D',
    registration: '>30D',
  },
  'Montenegro': {
    type: 'duration',
    period: '90D',
    registration: '24H',
  },
  'Morocco': {
    type: 'duration',
    period: '3M',
  },
  'Mozambique': {
    type: 'duration',
    period: '30D',
    voa: true,
  },
  'Myanmar': {
    type: 'duration',
    period: '28D',
    evisa: true,
  },
  'Namibia': {
    type: 'duration',
    period: '3M',
  },
  'Nauru': {
    type: 'visa',
  },
  'Nepal': { // ADDITIONAL RESTICTIONS
    type: 'duration',
    period: '90D',
    voa: true,
  },
  'New Zealand': { // ETA
    type: 'duration',
    period: '3M',
  },
  'Niger': {
    type: 'visa',
  },
  'Nigeria': {
    type: 'visa',
  },
  'North Macedonia': {
    type: 'duration',
    period: '90D',
    registration: '24H',
  },
  // Norway is weird
  'Oman': {
    type: 'duration',
    period: '30D',
    evisa: true,
  },
  'Pakistan': { // HOW LONG
    type: 'duration',
    evisa: true,
  },
  'Palau': {
    type: 'duration',
    period: '1Y',
  },
  'Panama': {
    type: 'duration',
    period: '180D',
  },
  'Papua New Guinea': {
    type: 'duration',
    period: '60D',
    extention: '90D',
    evisa: true,
    voa: true,
  },
  'Paraguay': {
    type: 'duration',
    period: '90D',
    voa: true,
  },
  'Peru': {
    type: 'duration',
    period: '183D',
    voa: true,
  },
  'Philippines': {
    type: 'duration',
    period: '30D',
  },
  // Poland
  'Qatar': {
    type: 'duration',
    period: '30D',
  },
  'Romania': {
    type: 'period',
    days: 90,
    period: '180D',
  },
  'Russia': {
    type: 'visa',
    registration: '>7D',
  },
  'Rwanda': {
    type: 'duration',
    period: '30D',
    evisa: true,
    voa: true,
  },
  'Saint Kitts and Nevis': {
    type: 'duration',
    period: '3M',
  },
  'Saint Lucia': {
    type: 'duration',
    period: '6W',
  },
  'Saint Vincent and the Grenadines': {
    type: 'duration',
    period: '6M',
  },
  'Samoa': {
    type: 'duration',
    period: '60D',
  },
  'San Marino': { // HOW LONG
    type: 'duration',
  },
  'São Tomé and Príncipe': {
    type: 'duration',
    period: '15D',
  },
  'Saudi Arabia': {
    type: 'duration',
    period: '90D',
    evisa: true,
    voa: true,
  },
  'Senegal': {
    type: 'duration',
    period: '90D',
  },
  'Serbia': {
    type: 'period',
    days: 90,
    period: '180D',
  },
  'Seychelles': {
    type: 'duration',
    period: '3M',
    extension: '1Y',
    voa: true,
  },
  'Sierra Leone': { // HOW LONG
    type: 'duration',
    voa: true,
  },
  'Singapore': {
    type: 'duration',
    period: '90D',
  },
  // Slovokia REGISTRATION
  'Solomon Islands': { // MONTHS WITHIN 12
    type: 'period',
    days: 90,
    period: '12M',
  },
  'Somalia': { // HOW LONG
    type: 'duration',
    period: '30D',
    voa: true,
  },
  'South Africa': {
    type: 'duration',
    period: '90D',
  },
  'South Sudan': { // HOW LONG
    type: 'duration',
    evisa: true,
  },
  'Sri Lanka': {
    type: 'duration',
    period: '30D',
    evisa: true,
    voa: true,
  },
  'Sudan': {
    type: 'visa',
    registration: '3D',
  },
  'Suriname': {
    type: 'duration',
    period: '90D',
    evisa: true,
  },
  'Syria': {
    type: 'visa',
    registration: '15D',
  },
  'Tajikistan': {
    type: 'duration',
    period: '45D',
    evisa: true,
  },
  'Tanzania': {
    type: 'duration',
    period: '3M',
    evisa: true,
    voa: true,
  },
  'Thailand': {
    type: 'duration',
    period: '30D',
  },
  'Timor-Leste': {
    type: 'duration',
    period: '30D',
    voa: true,
  },
  'Togo': {
    type: 'duration',
    period: '7D',
    voa: true,
  },
  'Tonga': {
    type: 'duration',
    period: '31D',
    voa: true,
  },
  'Trinidad and Tobago': {
    type: 'duration',
    period: '90D',
  },
  'Tunisia': {
    type: 'duration',
    period: '90D',
  },
  'Turkey': {
    type: 'duration',
    period: '90D',
  },
  'Turkmenistan': {
    type: 'visa',
  },
  'Tuvalu': {
    type: 'duration',
    period: '1M',
    extension: '3M',
    voa: true,
  },
  'Uganda': {
    type: 'duration',
    period: '90D',
    evisa: true,
    voa: true,
  },
  'Ukraine': {
    type: 'period',
    days: 90,
    period: '180D',
  },
  'United Arab Emirates': {
    type: 'duration',
    period: '30D',
    voa: true,
  },
  'United Kingdom': {
    type: 'duration',
    period: '6M',
  },
  'Uruguay': {
    type: 'duration',
    period: '3M',
  },
  'Uzbekistan': {
    type: 'duration',
    period: '30D',
    registration: '3D',
  },
  'Vanuatu': {
    type: 'duration',
    period: '30D',
    extension: '120D',
  },
  'Venezuela': {
    type: 'visa',
  },
  'Vietnam': {
    type: 'duration',
    period: '30D',
    evisa: true,
  },
  'Yemen': {
    type: 'visa',
    registration: '0D',
  },
  'Zambia': {
    type: 'duration',
    period: '90D',
    evisa: true,
    voa: true,
  },
  'Zimbabwe': {
    type: 'duration',
    period: '3M',
    evisa: true,
    voa: true,
  },

  // Subzones
  'Cayman Islands': {
    type: 'duration',
    period: '6M',
  },

  'Falkland Islands (Islas Malvinas)': {
    type: 'duration',
    period: '4W',
  },

  'Faroe Islands': {
    type: 'duration',
    period: '90D',
  },

  'Isle of Man': {
    type: 'duration',
    period: '30D',
  },

  // Misc
  'Antarctica': {
    type: 'visa',
    info: 'Permit required',
  },

  'Svalbard and Jan Mayen': {
    type: 'visafree',
  },

  'Portugal': {
    type: 'visafree',
  },
};

for (let v of Object.values(VISA_INFO)) {
  for (let f of ['period', 'extension', 'registration']) {
    if (v[f]) v[f] = Duration.fromISO('P' + v[f]);
  }
}

/* ** REMEMBER **
  Update this in the frontend as well */
const ZONES = {
  'Åland Islands': 'Schengen',
  'Aruba': 'NL-K',
  'Austria': 'Schengen',
  'Belgium': 'Schengen',
  'Bonaire': 'NL-K',
  'Bulgaria': 'Schengen',
  'Croatia': 'Schengen',
  'Cuba': 'United States',
  'Curaçao': 'NL-K',
  'Czechia': 'Schengen',
  'Denmark': 'Schengen',
  'El Salvador': 'CA-4',
  'Estonia': 'Schengen',
  'Finland': 'Schengen',
  'France': 'Schengen',
  'Germany': 'Schengen',
  'Greece': 'Schengen',
  'Guatemala': 'CA-4',
  'Honduras': 'CA-4',
  'Hungary': 'Schengen',
  'Iceland': 'Schengen',
  'Italy': 'Schengen',
  'Israel': 'Israel+',
  'Latvia': 'Schengen',
  'Liechtenstein': 'Schengen',
  'Lithuania': 'Schengen',
  'Luxembourg': 'Schengen',
  'Malta': 'Schengen',
  'Netherlands': 'Schengen',
  'Nicaragua': 'CA-4',
  'Norway': 'Schengen',
  'Palestine': 'Israel+',
  'Poland': 'Schengen',
  'Portugal': 'Schengen',
  'Romania': 'Schengen',
  'Slovakia': 'Schengen',
  'Slovenia': 'Schengen',
  'Spain': 'Schengen',
  'Sweden': 'Schengen',
  'Switzerland': 'Schengen',
};

function getZone(country, date) {
  if (country == 'Portugal' && date > DateTime.utc(2024, 10, 3)) return 'Portugal';
  if (country == 'Croatia' && date < DateTime.utc(2023, 1, 1)) return 'Croatia';
  if (country == 'Romania' && date < DateTime.utc(2024, 3, 31)) return 'Romania';
  if (country == 'Bulgaria' && date < DateTime.utc(2024, 3, 31)) return 'Bulgaria';
  return ZONES[country] || country;
}

exports.getZone = getZone;

const MY_VISAS = {
  'Antarctica': {
    'visas': [
      {
        start: '2023-10-01',
        end: '2023-12-01',
      },
    ],
  },
  'Burkina Faso': {
    'visas': [
      {
        start: '2018-07-31',
        end: '2023-07-30',
      },
    ],
  },
  'Nigeria': {
    'visas': [
      {
        start: '2019-11-19',
        end: '2021-11-18',
      },
    ],
  },
};

exports.VISA_INFO = VISA_INFO;
exports.MY_VISAS = MY_VISAS;
exports.ZONES = ZONES;
