'use strict';

const { DateTime } = require('luxon');
const { lookupAddr } = require('../calendar.js');
const { MONTH, DATE, DATE_SEP, YEAR, TIME, readDate, parseAmount } = require('../regex.js');

exports.parse = async (args, db) => {
  const { body, subject, from } = args;
  const $ = body;

  if (from !== 'mail@mail.quandoo.com') return null;

  const m = new RegExp(/Your reservation at (?<loc>.+) is confirmed/, 'iu').exec(subject);
  if (!m) return null;

  const venue = m.groups.loc;

  let startDate = null;
  let startTime = null;
  let addr = null;

  for (let tbody of $('tbody')) {
    const firstTag = $($('td', tbody).get(0)).text().trim();
    const secondTag = $($('td', tbody).get(1)).text().trim();

    if (firstTag === 'DATE') startDate = secondTag;
    if (firstTag === 'TIME') startTime = secondTag;
    if (firstTag === venue) addr = await lookupAddr(secondTag, db);
  }

  startDate = DateTime.fromFormat(startDate + ' ' + startTime, 'dd.MM.yyyy HH:mm', { zone: addr.tz });

  return {
    category: 'events',
    start: {
      ...addr,
      date: startDate,
      title: `Reservation at ${venue}`,
    },
    end: {
      ...addr,
      date: startDate.plus({ hours: 1 }),
    },
  }
};

const TIME_PREAMBLE = ' \\( (?:from|until)? ';

const DATE_FORMATS = [
  MONTH + DATE + DATE_SEP + YEAR + TIME_PREAMBLE + TIME,
];

function parseDate(text) {
  for (let phrase of DATE_FORMATS) {
    phrase = phrase.replace(/ /g, '\\s*');
    const regexp = new RegExp(phrase, 'i');
    const m = regexp.exec(text);
    if (m) return readDate(m);
  }

  return null;
}
