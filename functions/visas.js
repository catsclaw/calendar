const { DateTime, Interval, Duration } = require('luxon');
const { VISA_INFO, MY_VISAS, getZone } = require('./visaInfo');

function toLuxon(x) {
  return DateTime.fromSeconds(x.seconds, { zone: 'utc' });
}

function toLuxonISO(x) {
  return DateTime.fromISO(x, { zone: 'utc' });
}

exports.toLuxon = toLuxon;
exports.toLuxonISO = toLuxonISO;

async function getRanges(startDate, zones, db, t) {
  const zoneStartDates = [];
  let zoneStartDate = null;
  if (zones) {
    zones.forEach(z => zoneStartDates.push(getZoneLookback(z, startDate)));
    zoneStartDate = DateTime.min(...zoneStartDates);
  } else {
    zoneStartDate = startDate;
  }

  async function fetchResults(collection, start) {
    let query = db.collection(collection).where('endDate', '>=', start.toJSDate())
    if (zones) query = query.where('zone', 'in', Array.from(zones));

    let docs = null;
    if (t) {
      docs = await t.get(query);
    } else {
      docs = await query.get();
    }

    const res = {};
    docs.forEach(d => {
      const data = d.data();
      data.id = d.id;
      res[d.id] = data;
    });
    return res;
  }

  const [ranges, calculations] = await Promise.all([
    await fetchResults('ranges', zoneStartDate),
    await fetchResults('visaCalc', startDate),
  ]);
  return { ranges, calculations };
}

exports.getRanges = getRanges;

async function correctZones(db) {
  const docs = await db.collection('ranges').get();
  const promises = [];
  docs.forEach(doc => {
    const data = doc.data();
    const endDate = toLuxon(data.endDate);
    if (data.zone === getZone(data.country, endDate)) return;
    async function fix() {
      data.zone = getZone(data.country, endDate);
      await doc.ref.set(data);
    }

    promises.push(fix());
  });
  await Promise.all(promises);
}

exports.correctZones = correctZones;

async function setCalculations(create, destroy, db, t) {
  for (let [id, c] of Object.entries(create)) {
    const ref = db.collection('visaCalc').doc(id);
    if (t) {
      await t.set(ref, c);
    } else {
      await ref.set(c);
    }
  }
  for (let id of destroy) {
    const ref = db.collection('visaCalc').doc(id);
    if (t) {
      await t.delete(ref);
    } else {
      await ref.delete();
    }
  }
}

exports.setCalculations = setCalculations;

function calculateAvailableDays(ranges, travelDays) {
  const res = {};
  for ([id, r] of Object.entries(ranges)) {
    const startDate = toLuxon(r.startDate);
    const endDate = toLuxon(r.endDate);

    const func = getCalcFunction(r.country, startDate);
    res[id] = {
      ...func(startDate, endDate, travelDays),
      startDate: startDate.toJSDate(),
      endDate: endDate.toJSDate(),
    };
  }
  return res;
}

exports.calculateAvailableDays = calculateAvailableDays;

// Count x days in the past y
function checkPeriod(zone, startDate, endDate, days, period, travelDays) {
  const startPeriodDate = startDate.minus(period);
  const startEndPeriodDate = endDate.minus(period);

  let daysAtStart = 0;
  let daysAtEnd = 0;
  travelDays[zone].forEach(t => {
    const date = toLuxonISO(t);
    if (startPeriodDate <= date && date < startDate) daysAtStart += 1;
    if (startEndPeriodDate <= date && date <= endDate) daysAtEnd += 1;
  });
  return {
    start: days - daysAtStart,
    end: days - daysAtEnd - 1,
  };
}

// Check duration
function checkDuration(zone, startDate, endDate, duration, travelDays) {
  const startDurationDate = startDate.minus(duration);
  let daysAtStart = 0;
  let date = startDate.minus({days: 1});
  while (date >= startDurationDate) {
    if (!travelDays[zone].has(date.toISODate())) break;
    daysAtStart += 1;
    date = date.minus({days: 1});
  }

  duration = duration.as('days');
  const startDays = duration - daysAtStart;
  const endDays = startDays - Interval.fromDateTimes(startDate, endDate).length('days');
  return {
    start: startDays,
    end: endDays,
    error: endDays < 0,
    zone,
  }
}

function checkVisas(zone, startDate, endDate, visas) {
  visas = (visas || {}).visas || [];
  for (let v of visas) {
    const s = toLuxonISO(v.start);
    const e = toLuxonISO(v.end);
    if (s > startDate || startDate > e) continue;
    const daysAtEnd = Interval.fromDateTimes(endDate, e).length('days');
    return {
      start: Interval.fromDateTimes(startDate, e).length('days') + 1,
      end: daysAtEnd,
      error: daysAtEnd < 0,
      zone,
    };
  }

  return {
    info: 'No valid visa',
    error: true,
    zone,
  };
}

function checkIreland(startDate, endDate, travelDays) {
  // Ireland extended COVID permission until 20 Sept, 2021
  const specialStart = DateTime.utc(2020, 5, 1);
  const specialEnd = DateTime.utc(2021, 9, 20);
  if (startDate >= specialStart && endDate <= specialEnd) {
    return {
      start: Interval.fromDateTimes(startDate, specialEnd).length('days') + 1,
      end: Interval.fromDateTimes(endDate, specialEnd).length('days'),
      info: 'Special COVID dispensation',
      zone: 'Ireland',
    }
  }
  const func = getCalcFunctionFromZone('Ireland');
  return func(startDate, endDate, travelDays);
}

function checkKingdomOfNetherlands(startDate, endDate, country, travelDays) {
  // The basic rules seem to be you can stay for 90 out of 180, but no
  // more than 30 consecutive days
  let date = startDate.minus({ days: 180 });
  const startEndPeriod = endDate.minus({ days: 180 });

  let inCountryDays = 0;
  let inCountryDaysAtStart = 0;
  let inCountryDaysAtEnd = 0;
  let inCountryMax = 0;

  let intervalDaysForStart = 0;
  let intervalDaysForEnd = 0;

  while (date <= endDate) {
    const iso = date.toISODate();
    const inCountry = travelDays[country].has(iso);
    const inZone = travelDays['NL-K'].has(iso);

    if (inCountry) {
      inCountryDays += 1;
    } else {
      inCountryDays = 0;
    }
    if (+date === +startDate.minus({ days: 1})) inCountryDaysAtStart = inCountryDays;
    if (+date === +endDate) inCountryDaysAtEnd = inCountryDays;

    if (date < startDate) {
      if (inZone) intervalDaysForStart += 1;
    } else {
      inCountryMax = Math.max(inCountryMax, inCountryDays);
    }
    if (inZone && date > startEndPeriod) intervalDaysForEnd += 1;
    date = date.plus({ days: 1 });
  }

  const intervalAtStart = 90 - intervalDaysForStart;
  const intervalAtEnd = 90 - intervalDaysForEnd;

  let error = false;
  let info = null;
  if (inCountryMax > 30) {
    info = `Can only stay 30 consecutive days (stayed ${inCountryMax})`;
    error = true;
  } else if (intervalAtEnd < 0) {
    error = true;
  }

  const r = {
    start: Math.min(30 - inCountryDaysAtStart, intervalAtStart),
    end: Math.min(30 - inCountryDaysAtEnd, intervalAtEnd),
    error,
    zone: 'NL-K',
  }
  if (info) r.info = info;
  return r;
}

function checkUSA(startDate, endDate, travelDays) {
  if (endDate.year < 2019) {
    return {
      start: 365,
      end: 365,
    }
  }

  const startPeriodDate = startDate.startOf('year');
  const startEndPeriodDate = endDate.startOf('year');

  let daysAtStart = 0;
  travelDays['United States'].forEach(t => {
    const date = toLuxonISO(t);
    if (startPeriodDate <= date && date < startDate) daysAtStart += 1;
  });
  let daysAtEnd = null;
  if (startDate.year === endDate.year) {
    daysAtEnd = Interval.fromDateTimes(startDate, endDate).length('days') + daysAtStart;
  } else {
    daysAtEnd = Interval.fromDateTimes(endDate.startOf('year'), endDate).length('days');
  }

  const startPeriod = Math.floor(Interval.fromDateTimes(startDate.startOf('year'), startDate.endOf('year')).length('days') + 0.00001) - 330;
  const endPeriod = Math.floor(Interval.fromDateTimes(endDate.startOf('year'), endDate.endOf('year')).length('days') + 0.00001) - 330;
  return {
    start: startPeriod - daysAtStart,
    end: endPeriod - daysAtEnd,
    error: (endPeriod - daysAtEnd) < 0,
    zone: 'United States',
  };
}

function getZoneLookback(zone, date) {
  if (zone === 'United States') return date.startOf('year');
  if (zone === 'Schengen') return date.minus({ days: 180 });
  if (zone === 'CA-4') return date.minus({ months: 3 });
  if (zone === 'NL-K') return date.minus({ days: 365 });

  const visaInfo = VISA_INFO[zone] || {};
  if (visaInfo.type === 'duration') return date.minus(visaInfo.period);
  if (visaInfo.type === 'visa') return date;
  if (visaInfo.type === 'period') return date.minus(visaInfo.period);
  if (visaInfo.type === 'visafree') return date;
  return date.minus({ years: 10 }); // Completely safe, if paranoid
}

function getCalcFunctionFromZone(zone) {
  const visaInfo = VISA_INFO[zone];
  if (!visaInfo) {
    return (s, e, t) => {
      return {
        info: `Unknown zone: ${zone}`,
        error: true,
        zone,
      };
    };
  }
  if (visaInfo.type === 'duration') return (s, e, t) => checkDuration(zone, s, e, visaInfo.period, t);
  if (visaInfo.type === 'visa') return (s, e, t) => checkVisas(zone, s, e, MY_VISAS[zone], t);
  if (visaInfo.type === 'period') return (s, e, t) => checkPeriod(zone, s, e, visaInfo.days, visaInfo.period, t);
  if (visaInfo.type === 'visafree') return (s, e, t) => {
    return {
      visafree: true,
      zone,
    };
  };

  return (s, e, t) => {
    return {
      info: `Unknown type: ${visaInfo.type}`,
      error: true,
      zone,
    };
  };
}

function getCalcFunction(country, date) {
  if (country === 'United States') return checkUSA;
  if (country === 'Ireland') return checkIreland;

  const zone = getZone(country, date);
  if (zone === 'Israel+') return (s, e, t) => checkDuration('Israel+', s, e, Duration.fromISO('P3M'), t);
  if (zone === 'NL-K') return (s, e, t) => checkKingdomOfNetherlands(s, e, country, t);
  if (zone === 'Schengen') return (s, e, t) => checkPeriod('Schengen', s, e, 90, Duration.fromISO('P180D'), t);
  if (zone === 'CA-4') return (s, e, t) => checkDuration('CA-4', s, e, Duration.fromISO('P3M'), t);
  return getCalcFunctionFromZone(zone);
}
