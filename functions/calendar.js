'use strict';

const { google } = require('googleapis');
const { Client } = require("@googlemaps/google-maps-services-js");
const geoTz = require('geo-tz');
const { DateTime } = require('luxon');
const { toLuxon, toLuxonISO } = require('./visas');

const calendar = google.calendar('v3');
const gmail = google.gmail('v1');
const geoClient = new Client({});
const OAuth2 = google.auth.OAuth2;

const CALENDAR_ID = '1016jeolmgaikoea566ksu5ieo@group.calendar.google.com';
const GEO_KEY = 'AIzaSyBLT_shZHPlPZOb5TRS5kFtp7sTUe7OHpI';

const googleCredentials = require('./google-calendar-creds.json');
const oAuth2Client = new OAuth2(
  googleCredentials.client_id,
  googleCredentials.client_secret,
  googleCredentials.redirect_uris[0]
);
oAuth2Client.setCredentials({
  refresh_token: googleCredentials.refresh_token,
});
google.options({ auth: oAuth2Client });

function b64enc(s) {
  return Buffer.from(s).toString('base64').replaceAll('+', '-').replaceAll('/', '_');
}

function b64dec(s) {
  return Buffer.from(s.replaceAll('_', '/').replaceAll('-', '+'), 'base64').toString();
}

exports.getScheduled = async (args) => {
  // If we pass a lat/lng, we need to convert the times from UTC to the local timezone
  // If not, they're already correct
  let startDate = toLuxonISO(args.startDate);
  let endDate = toLuxonISO(args.endDate);
  let tz = args.tz || null;
  if (args.latLng) {
    tz = getTimeZoneFromLatLng(args.latLng);
    startDate = startDate.setZone(tz, { keepLocalTime: true });
    endDate = endDate.setZone(tz, { keepLocalTime: true });
  }
  const r = await calendar.events.list({
    calendarId: CALENDAR_ID,
    singleEvents: true,
    timeMax: endDate.toISO(),
    timeMin: startDate.toISO(),
  });

  return {
    tz,
    scheduled: r.data.items.map(x => {
      const exProps = (x.extendedProperties || {}).private || {};

      let startLocation = x.location;
      let endLocation = exProps.endLocation || startLocation;
      if (exProps.country) {
        startLocation = `${exProps.city}, ${exProps.country}`;
        if (exProps.endCountry) {
          endLocation = `${exProps.endCity}, ${exProps.endCountry}`;
        } else {
          endLocation = startLocation;
        }
      }
      return {
        id: x.id,
        start: {
          date: x.start.dateTime,
          title: x.summary,
          location: startLocation,
          tz: x.start.timeZone,
        },
        end: {
          date: x.end.dateTime,
          title: exProps.endTitle || null,
          location: endLocation,
          tz: x.end.timeZone,
        },
        category: exProps.category || 'events',
        link: x.source ? x.source.url : null,
      };
    }),
  };
}

exports.lookupAddr = async function(addr, db) {
  addr = addr.trim();
  let placeReq = null;
  while (true) {
    placeReq = await geoClient.findPlaceFromText({
      params: {
        input: addr,
        inputtype: 'textquery',
        language: 'en',
        fields: ['place_id'],
        key: GEO_KEY
      },
    });

    if (placeReq.data.candidates.length) break;

    // Cut out line by line and retry?
    addr = addr.split('\n').slice(1).join('\n').trim();
    if (!addr) return {};
  }

  const placeId = placeReq.data.candidates[0]['place_id'];
  const countryReq = await geoClient.placeDetails({
    params: {
      place_id: placeId,
      language: 'en',
      fields: ['address_components', 'formatted_address', 'name'],
      key: GEO_KEY
    },
  });

  const result = {};
  let postalTown = null;
  let locality = null;
  let adminArea1 = null;

  for (let d of countryReq.data.result.address_components) {
    if (d.types.includes('country')) result.country = d.long_name;
    if (d.types.includes('administrative_area_level_2')) result.city = d.long_name;
    if (d.types.includes('postal_town')) postalTown = d.long_name;
    if (d.types.includes('locality')) locality = d.long_name;
    if (d.types.includes('administrative_area_level_1')) adminArea1 = d.short_name;
  }

  result.city = locality || postalTown || result.city;
  if (result.country === 'United States') result.city += `, ${adminArea1}`;

  if (result.city && result.country) {
    result.tz = await exports.getTimeZoneFromLocation(`${result.city}, ${result.country}`, db);
  }

  result.streetAddress = countryReq.data.result.formatted_address;
  result.name = countryReq.data.result.name;

  return result;
};

const CALENDAR_COLORS = {
  events: 3,
  hotels: 1,
  travel: 6,
};

exports.getTimeZoneFromLocation = async (location, db) => {
  const tzDoc = await db.collection('tz').doc(location).get();
  if (tzDoc.exists) {
    let data = tzDoc.data();
    if (toLuxon(data.expires) > DateTime.utc()) return data.tz;
  }

  const latLngReq = await geoClient.geocode({
    params: {
      address: location,
      key: GEO_KEY,
    },
  });
  const latLng = latLngReq.data.results[0].geometry.location;
  const tz = getTimeZoneFromLatLng(latLng);
  await tzDoc.ref.set({
    tz,
    expires: DateTime.utc().plus({ months: 3 }).toJSDate(),
  });
  return tz;
}

function getTimeZoneFromLatLng(latLng) {
  return geoTz(latLng.lat, latLng.lng)[0];
}
